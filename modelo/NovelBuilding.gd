extends Node

#LABEL: Etiqueta de texto.
#SPRITE: nodo de imagen, png o jpe.


#Esta archivo está creado con la finalidad de controlar y crear los efectos o animaciones de la novela.
#Se basa en los labels y sprites, y sus movimientos, posiciones, y transiciones para aparecer y desaparecer

#La gran mayoria de estos métodos usan la recursividad (llamarse a si mismos)
#para poder se controlados conforme pasa el tiempo, es decir, después de cierta cantidad
#de llamados, se detienen). Es muy necesario aclarar su detenimiento debido a que puede recargar
#el dispositivo donde corre la aplicación.

#La mayoría de los siguientes métodos utilizan un parámetro llamado PREV.
#que hace directa referencia al uso de:
#yield(get_tree().create_timer(prev), "timeout")
#Esto hace una pausa o "timeout" en la aplicación, por la cantidad de tiempo descrita en 'prev',
#medida en segundos (float).
#Esto premite controlar cuando sucedera el método correspondiente en el flujo de la escena.


#Variale que coordina la secuencia general de cada capítulo.
var interac:String = ""
#Control particular para ciertas ocaciones.
var show:bool = false
#Control para el efecto animado de parpadio de una imagen ("sprite")
var stop_1:bool = false
var stop_2:bool = false

var testTime = 45
var barSize = 300
var change_v = 1
var endLoop = false


#Método para el crear un efecto de movimiento para los sprites.
#Dando un punto de partida y un punto a donde llegar.
#Particularmente este método sirve para mover el sprite de izquierda a derecha.
#Más que movimiento, sirve preferiblemente como transición del sprite a escena.
func _movement_x_left_right(sp:Sprite, inicio:int, fin:int, speed:float, prev:float, y):
	if(prev!=0):
		yield(get_tree().create_timer(prev), "timeout")
		prev=0
	yield(get_tree().create_timer(0.003), "timeout")
	inicio-=speed
	sp.position.y = y
	sp.position.x = inicio
	if(inicio<=fin):
		print(str("end movement:",sp.name))
		return true
	else:
		return _movement_x_left_right(sp,inicio,fin, speed, prev, y)

#Método inverso al anterior, particularizando la dirección de derecha a izquierda.
func _movement_x_right_left(sp:Sprite, inicio:int, fin:int, speed:float, prev:float, y:int):
	if(prev!=0):
		yield(get_tree().create_timer(prev), "timeout")
		prev=0
	yield(get_tree().create_timer(0.003), "timeout")
	inicio+=speed
	sp.position.y = y
	sp.position.x = inicio
	if(inicio<=fin):
		print(str("end movement:",sp.name))
		return true
	else:
		return _movement_x_right_left(sp,inicio,fin, speed, prev, y)

#Método para el control de transiciones de los sprite.
#Crea una suave transición enfocada en la opacidad.
#Usa la recursividad, dando una espera de 0.003 segundos en volverse a llamar,
#así tarda la suficiente para que la transición no sea inmediata.
#Cabe recalcar que este es uno de los métodos que más se usa.
#ya que se utiliza para casi todas las transiciones de los sprites.
#Aquí vemos el uso de la variable global interac, que es la controla cuando se detiene la recursividad.
#Dicha variable estipula que interacción se hará después de que aparezca la sprite mandada por parámetro (sp)
func _counter_showup(inicio:int, fin:int, vel:int, sp:Sprite, prev:float, controlF:String):
	if(prev!=0):
		print(str("prev! esperando: ",sp.name))
		yield(get_tree().create_timer(prev), "timeout")
		prev=0
	yield(get_tree().create_timer(0.003), "timeout")
	inicio+=vel
	if(sp):
		sp.show()
		sp.self_modulate.a8 = inicio
	if(inicio>=fin):
		interac = controlF
		return
	else:
		return _counter_showup(inicio, fin, vel, sp,0, controlF)

#Método espejo del anterior, que funciona de forma invertidad al hacer la "desaparición" de la sprite
#mandada por parámetro (sp).
func _counter_showoff(inicio:int, fin:int, vel:int, sp:Sprite, controlF:String):
	yield(get_tree().create_timer(0.003), "timeout")
	inicio-=vel
	sp.show()
	sp.self_modulate.a8 = inicio
	if(inicio<=fin):
		sp.hide()
		print(str("showing off: ", sp.name))
		interac = controlF
		return 0
	else:
		return _counter_showoff(inicio, fin, vel, sp, controlF)

#En la novela existe un punto donde es necesario crear un efecto de parpadeo.
#Se logra por medio de la mencionada recursividad y la variable booleana 'show'.
#cada vez que este método se llama a si mismo, esta variable se invierte (de tru a false y viceversa).
#Cada valor cambia la transparencia de la sprite que se llama como parámetro.
#Esto crea un efecto de parpadeo para el usuario.
func blink_op(sp:Sprite):
	show = !show
	yield(get_tree().create_timer(0.5), "timeout")
	if(sp):
		if(show):
			#sp.hide()
			sp.self_modulate.a8 = 100
		if(!show):
			#sp.show()
			sp.self_modulate.a8 = 255
		if(stop_1):
			return 0
			print(str("end blinking"))
			sp.self_modulate.a8 = 255
			NovelBuilding.interac = "CAP1.3"
	return blink_op(sp)

func blink_op_v2(sp:Sprite):
	show = !show
	yield(get_tree().create_timer(0.5), "timeout")
	if(sp):
		if(show):
			#sp.hide()
			sp.self_modulate.a8 = 100
		if(!show):
			#sp.show()
			sp.self_modulate.a8 = 255
		if(stop_2):
			return 0
			print(str("end blinking"))
			sp.self_modulate.a8 = 255
	return blink_op(sp)

#Este método controla los diálogos. Por médio de un label y un sprite
#La imagen (sp_diag)es un "nube" de texto usado recurrentemente en los comics. Este se recibe como parámetro,
#pero siempre será la misma, se hace así debido a que solo archivos ligados a una vista puede enlazar sprites, 
#y no es el caso de este archivo, el cual solo es una API para el manejo general de la novela.
#El label va cuadrado usualmente adentro del sprite de nube de texto, esto es totalmente manejable por 
#medio de las posX_tx, posY_tx para el label, y bgX, bgY para el sprite.
#El texto deseado se escribe como parámetro y se iguala al valor de texto del label.
func _dialog_v1(txt:String, posX_tx:float, posY_tx:float, bgX:float, bgY:float, prev:float, sp_diag:Sprite, lb_diag:Label):
	if(prev!=0):
		yield(get_tree().create_timer(prev), "timeout")
		prev=0
	sp_diag.show()
	sp_diag.position.y = bgX
	sp_diag.position.x = bgY
	lb_diag.show()
	lb_diag.add_color_override("font_color", Color(0,0,0))
	print(str("diálogo: ", lb_diag))
	lb_diag.text =txt
	var pos:Vector2 = Vector2(posX_tx,posY_tx)
	lb_diag.set_position(pos)

#Método simplificado para "desaparecer" una sprite, sin transición.
func _counter_showoff_2(prev:float, sp:Sprite):
	yield(get_tree().create_timer(prev), "timeout")
	sp.hide()
	print(str("gone: ", sp.name))

#método simplificado para desaparecer un label.
func _counter_showoff_2_lb(prev:float, sp:Label):
	yield(get_tree().create_timer(prev), "timeout")
	sp.hide()
	print(str("gone: ", sp.name))

#Método hecho para contar el tiempo de los retos.
#Es decir, 30 segundos en retroceso para que el usuario termine el reto.
func test_time_counter(prev:float, limit:int, lab_timer:Label, base_v1:Sprite, base_v2:Sprite, ans:int):
	if(prev!=0):
		yield(get_tree().create_timer(prev), "timeout")
		prev=0
	yield(get_tree().create_timer(1.2), "timeout")
	testTime-=1
	barSize-=10
	if(lab_timer):
		lab_timer.text = str("Tiempo restante: ",testTime)
	print(str("Time left: ",testTime))
	print(str("test version: ",change_v))
	print(str("var endloop = ", endLoop))
	if(change_v==1 && base_v1 && base_v2):
			base_v1.show()
			base_v2.hide()
	if(change_v==2 && base_v1 && base_v2):
			base_v2.show()
			base_v1.hide()
	if(testTime <= 0):
		testTime = 45
		print(str("ended"))
		if(change_v==1):
			change_v=2
			return 0
		if(change_v==2):
			change_v=1
			return 0
	else:
		return test_time_counter(prev, limit,lab_timer, base_v1, base_v2, ans)

func test_time_counter_v6(prev:float, limit:int, lab_timer:Label, base_v1:Sprite, base_v2:Sprite, base_v3:Sprite, base_v4:Sprite, base_v5:Sprite, base_v6:Sprite, ans:int):
	if(prev!=0):
		yield(get_tree().create_timer(prev), "timeout")
		prev=0
	yield(get_tree().create_timer(1.2), "timeout")
	testTime-=1
	barSize-=10
	if(lab_timer):
		lab_timer.text = str("Tiempo restante: ",testTime)
	print(str("Time left: ",testTime))
	print(str("test version: ",change_v))
	print(str("var endloop = ", endLoop))
	if(change_v==1 && base_v1 && base_v2 && base_v3 && base_v4 && base_v5 && base_v6):
			base_v1.show()
			base_v2.hide()
			base_v3.hide()
			base_v4.hide()
			base_v5.hide()
			base_v6.hide()
	if(change_v==2 && base_v1 && base_v2 && base_v3 && base_v4 && base_v5 && base_v6):
			base_v1.hide()
			base_v2.show()
			base_v3.hide()
			base_v4.hide()
			base_v5.hide()
			base_v6.hide()
	if(change_v==3 && base_v1 && base_v2 && base_v3 && base_v4 && base_v5 && base_v6):
			base_v1.hide()
			base_v2.hide()
			base_v3.show()
			base_v4.hide()
			base_v5.hide()
			base_v6.hide()
	if(change_v==4 && base_v1 && base_v2 && base_v3 && base_v4 && base_v5 && base_v6):
			base_v1.hide()
			base_v2.hide()
			base_v3.hide()
			base_v4.show()
			base_v5.hide()
			base_v6.hide()
	if(change_v==5 && base_v1 && base_v2 && base_v3 && base_v4 && base_v5 && base_v6):
			base_v1.hide()
			base_v2.hide()
			base_v3.hide()
			base_v4.hide()
			base_v5.show()
			base_v6.hide()
	if(change_v==6 && base_v1 && base_v2 && base_v3 && base_v4 && base_v5 && base_v6):
			base_v1.hide()
			base_v2.hide()
			base_v3.hide()
			base_v4.hide()
			base_v5.hide()
			base_v6.show()
	if(testTime <= 0):
		testTime = 45
		print(str("ended"))
		if(change_v==1):
			change_v=2
			return 0
		if(change_v==2):
			change_v=3
			return 0
		if(change_v==3):
			change_v=4
			return 0
		if(change_v==4):
			change_v=5
			return 0
		if(change_v==5):
			change_v=6
			return 0
		if(change_v==6):
			change_v=1
			return 0
	else:
		return test_time_counter_v6(prev, limit,lab_timer, base_v1, base_v2, base_v3, base_v4, base_v5, base_v6, ans)

func testLooping(prev:float, limit:int, lab_timer:Label, base_v1:Sprite, base_v2:Sprite, ans:int):
	print(str("¿Terminar loop?", endLoop))
	yield(get_tree().create_timer(55), "timeout")
	print("checking")
	if(endLoop == false):
		print(str("restarting"))
		test_time_counter(prev, limit,lab_timer, base_v1, base_v2, ans)
		testLooping(prev, limit,lab_timer, base_v1, base_v2, ans)
	else:
		print("nope")
		return 0

func testLooping_v6(prev:float, limit:int, lab_timer:Label, base_v1:Sprite, base_v2:Sprite, base_v3:Sprite, base_v4:Sprite, base_v5:Sprite, base_v6:Sprite, ans:int):
	print(str("¿Terminar loop?", endLoop))
	yield(get_tree().create_timer(55), "timeout")
	print("checking")
	if(endLoop == false):
		print(str("restarting"))
		test_time_counter_v6(prev, limit,lab_timer, base_v1, base_v2, base_v3, base_v4, base_v5, base_v6, ans)
		testLooping_v6(prev, limit,lab_timer, base_v1, base_v2, base_v3, base_v4, base_v5, base_v6, ans)
	else:
		print("nope")
		return 0
