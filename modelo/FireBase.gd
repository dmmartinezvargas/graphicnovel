extends Node

#Clave traída de FireBase en la Configuración del proyecto, label:"Clave de API de la web"
#Para hacer modificaciones y consultar la bd.
const API_KEY := "AIzaSyB0lK83lKgTH9jbxqZ_4hS4s-ZA28Ed9r4"
#Nombre del proyecto de FireBase
const PROJECT_ID: ="tg-novel"

#Claves de acceso necesarias para los métodos HTTP, es decir, contruir los links de consulta.
const REGISTER_URL := "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=%s" % API_KEY
const LOGIN_URL := "https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=%s" % API_KEY
const FIRESTORE_URL :="https://firestore.googleapis.com/v1/projects/%s/databases/(default)/documents/" % PROJECT_ID

#Diccionario para recibi la información de usuario registrados y usarla para los métodos HTTP.
var user_info := {}

func _get_user_info(result: Array) -> Dictionary:
	var result_body := JSON.parse(result[3].get_string_from_ascii()).result as Dictionary
	return {
		"token": result_body.idToken,
		"id":result_body.localId,
		"email":result_body.email
	}
#Headers que se envían junto a las consultas a FireBase
func _get_request_headers() -> PoolStringArray:
	return PoolStringArray([
		"Content-Type: application/json",
		"Authorization: Bearer %s" %user_info.token #El token se reenvía para tener acceso a la base de datos.
	])

#Registro(Método post) en la aplicación (bd), usando campos de email y password para hacer login
func register(email: String, password:String, http:HTTPRequest) -> void:
	var body :={
		"email":email,
		"password":password,
	}
	http.request(REGISTER_URL, [], false, HTTPClient.METHOD_POST, to_json(body))
	var result : = yield(http, "request_completed") as Array
	
#	Si recibe respuesta correcta, devuelve "200", código para acceso correcto, en tal caso, la variable user_info, tomará los datos enviados.
	if result[1] == 200:
		user_info = _get_user_info(result)

#Login por medio de email y contraseña, con token de acceso para la base de datos.
func login(email:String, password:String, http:HTTPRequest) -> void:
	var body: = {
		"email": email,
		"password": password,
		"returnSecureToken": true
	}
	http.request(LOGIN_URL, [], false, HTTPClient.METHOD_POST, to_json(body))
	var result := yield(http, "request_completed") as Array
	#Similar al registro, user_info hereda la información de las credenciales, para futuro manejo de los otros métodos HTTP.
	if result[1] == 200:
		user_info = _get_user_info(result)
		

#------------------- CRUD PARA USUARIOS (DOCENTE Y ESTUDIANTE)----------------
#Todos los método funcionan de la misma manera:
#usan un método HTTP, usan los Headers para acceder correctamente a la bd y las direcciones(link) para introducir dichos métodos.
#Registro (Creación)
func save_document(path: String, fields: Dictionary, http: HTTPRequest) -> void:
	var document:= { "fields": fields}
	var body := to_json(document)
	var url := FIRESTORE_URL + path
	http.request(url, _get_request_headers(), false, HTTPClient.METHOD_POST, body)
	print("saved")

#Consulta
func get_document(path: String, http: HTTPRequest) -> void:
	var url:= FIRESTORE_URL+path
	http.request(url, _get_request_headers(), false,  HTTPClient.METHOD_GET)

#Actualizar
func update_document(path: String, fileds: Dictionary, http: HTTPRequest) -> void:
	var document := { "fields": fileds}
	var body :=to_json(document)
	var url := FIRESTORE_URL+path
	http.request(url, _get_request_headers(), false, HTTPClient.METHOD_PATCH, body)
	print("updated")

#Eliminar
func delete_document(path: String, http: HTTPRequest) -> void:
	var url := FIRESTORE_URL+path
	http.request(url, _get_request_headers(), false, HTTPClient.METHOD_DELETE)



