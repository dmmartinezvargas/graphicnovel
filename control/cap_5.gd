extends Node2D

onready var http: HTTPRequest = $HTTPRequest

onready var intro:Sprite = $inicio
onready var sara_familia:Sprite = $sara_familia
onready var sara_primer:Sprite = $sara_primer
onready var decision:Sprite = $decision
onready var result_1:Sprite = $result_1
onready var sara_idea:Sprite = $sara_idea
onready var juan_yuli:Sprite = $juan_yuli
onready var juan_yuli_abrazo:Sprite = $juan_yuli_abrazo
onready var juan_libreta:Sprite = $juan_libreta
onready var juan_primer:Sprite = $juan_primer
onready var tiempo_sol:Sprite = $tiempo_sol

onready var dialog_lb:Label = $dialog_lb
onready var dialog_bg:Sprite = $dialog_bg

onready var bg_:Sprite = $bg
onready var noti:Label = $noti
onready var prueba_diag:Label = $enunc
onready var pregunta_test_1:Sprite
onready var test_1_bg_v1: Sprite = $test_v1
onready var test_1_l:Button = $left
onready var test_1_r:Button = $right
onready var test_1_ace:Button = $accept
var test_1_:bool = false
var estudiante
var right_ans:int = 0
var test_1_current_ans:int = 1

var perfil :={
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http) #Aquí inicia el @Paso 1
	yield(get_tree().create_timer(4), "timeout")
	cap_5v1()

func cap_5v1():
	NovelBuilding._counter_showup(0,255,1,intro,0, "")
	NovelBuilding._counter_showup(0,255,1,sara_familia,9, "comment_1_v1")
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \nDebemos pensar en algo hija, no nos podemos \nquedar de brazos cruzados.",
		20,25,20, 5,13, dialog_bg,dialog_lb)

func comment_1_v1():
	NovelBuilding._dialog_v1("SARA: \nNo lo sé...",
		20,25,20, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v2"

func comment_1_v2():
	NovelBuilding._dialog_v1("ABUELA DE SARA: \nLas ventas siempre funcionan mientras haya ganas \ny no se fíe. Nosotras preparamos el salpicón.",
		20,25,20, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(3), "timeout")
	NovelBuilding._counter_showup(0,255,1,sara_primer,0.1, "cap5_v2")

func cap5_v2():
	NovelBuilding._dialog_v1("SARA: \nPero, ¿a dónde iré a vender?",
		20,25,20, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="_interact_1"

func _interact_1():
	dialog_bg.hide()
	dialog_lb.hide()
	result_1.hide()
	NovelBuilding._counter_showup(0,255,1,decision,0.5, "_interact_1_result")
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding._dialog_v1("SARA: \n¿A dónde iré a vender el salpicón?",
		20,15, 10, 5,4.5, dialog_bg,dialog_lb)

func _interact_1_result_wrong():
	NovelBuilding._counter_showup(0,255,1,result_1,0.5, "_interact_1")
	dialog_bg.hide()
	dialog_lb.hide()

func _interact_1_result_foward():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,sara_idea,0.5, "cap5_v3")
	NovelBuilding._dialog_v1("SARA: \nGracias. Lugar perfecto.",
		20,535,530, 5,2.5, dialog_bg,dialog_lb)

func cap5_v3():
	NovelBuilding._counter_showup(0,255,1,juan_yuli,0.5, "comment_2_v1")
	dialog_bg.hide()
	dialog_lb.hide()
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding._dialog_v1("JUAN: \n¿Y quién es el papá?",
		20,35,30, 5,2.5, dialog_bg,dialog_lb)

func comment_2_v1():
	NovelBuilding._dialog_v1("YULIANA: \nEso no importa ya, me dijo que si lo culpaba, iba a \nnegarlo todo. ¡Es un desgraciado!",
		20,35,30, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_2_v2"

func comment_2_v2():
	NovelBuilding._dialog_v1("JUAN: \nPara eso existen las pruebas de paternidad. Ve a \ndescansar, mañana iremos a la ciclovía para \nhablar de esto mejor.",
		20,35,30, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_2_v3"

func comment_2_v3():
	NovelBuilding._dialog_v1("JUAN: \nNo quiero que mamá nos escuche y se entere. \nAl menos, no todavía.",
		20,35,30, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_2_v4"

func comment_2_v4():
	NovelBuilding._dialog_v1("YULIANA: \nEstá bien.",
		20,35,30, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_2_v5"

func comment_2_v5():
	NovelBuilding._dialog_v1("JUAN: \nTranquila, mientras yo esté, no les va a faltar nada.",
		20,35,30, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="cap5_v4"

func cap5_v4():
	NovelBuilding._counter_showup(0,255,1,juan_yuli_abrazo,0.5, "cap5_v5")
	yield(get_tree().create_timer(2), "timeout")
	dialog_bg.hide()
	dialog_lb.hide()

func cap5_v5():
	NovelBuilding._counter_showup(0,255,1,juan_libreta,0.5, "reto_")

func reto_():
	dialog_bg.hide()
	dialog_lb.hide()
	print(str("TEST 5"))
	NovelBuilding._counter_showup(0,255,1,bg_, 1, "TEST1v2")
	yield(get_tree().create_timer(2), "timeout")
	prueba_diag.text ="Juan, nervioso, tumba sin querer un vaso en la \nmesa y moja un recibo. Los valores se borraron, \ny Juan no sabe cuanto debe pagar y hoy se vence \nel plazo de pago. No obstante hay una fórmula \npara calcular el valor a pagar: \n2(X^2) + 6X + 9 = 225. \nAyuda a Juan a escoger cual es el procedimiento \nadecuado, usando factorización."
	prueba_diag.show()
	test_1_ = true;
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding.endLoop = false
	NovelBuilding.change_v = 1
	reto_v2()

func reto_v2():
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var sprite_choose = rng.randi_range(0,100)
	print(sprite_choose)
	
	if(sprite_choose>= 0 and sprite_choose<=50):
		pregunta_test_1 = test_1_bg_v1
		right_ans=1
		print(str("pregunta v1"))
	if(sprite_choose>= 51 and sprite_choose<=100):
		pregunta_test_1 = test_1_bg_v1
		right_ans=1
		print(str("pregunta v2"))
	print(sprite_choose)
	
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding._counter_showup(0,255,1,pregunta_test_1, 2, "TEST1v2")
	
	if(test_1_):
		test_1_l.visible = true
		test_1_r.visible = true
		test_1_ace.visible = true

func _on_left_pressed():
	print(str("current: ",test_1_current_ans))
	if(test_1_):
		test_1_current_ans+=1
		if (test_1_current_ans>=7):
			test_1_current_ans=1
			
	if(test_1_current_ans==1):
		pregunta_test_1.position.x = 0
	if(test_1_current_ans==2):
		pregunta_test_1.position.x = -400
	if(test_1_current_ans==3):
		pregunta_test_1.position.x = -800
	if(test_1_current_ans==4):
		pregunta_test_1.position.x = -1200
	if(test_1_current_ans==5):
		pregunta_test_1.position.x = -1600
	if(test_1_current_ans==6):
		pregunta_test_1.position.x = -2000

func _on_right_pressed():
	print(str("current: ",test_1_current_ans))
	if(test_1_):
		test_1_current_ans-=1
		if (test_1_current_ans<=0):
			test_1_current_ans=6
			
	if(test_1_current_ans==1):
		pregunta_test_1.position.x = 0
	if(test_1_current_ans==2):
		pregunta_test_1.position.x = -400
	if(test_1_current_ans==3):
		pregunta_test_1.position.x = -800
	if(test_1_current_ans==4):
		pregunta_test_1.position.x = -1200
	if(test_1_current_ans==5):
		pregunta_test_1.position.x = -1600
	if(test_1_current_ans==6):
		pregunta_test_1.position.x = -2000

func _on_accept_pressed():
	if(test_1_):
		print(str("ANSWER: ", test_1_current_ans))
		#Para v1, respuesta es 1
		if(test_1_current_ans == right_ans):
			noti.text = "Respuesta correcta."
			print(str("CORRECTO v1"))
			perfil.puntajePrueba4 = {"stringValue": "CORRECTO"}
			perfil.codigoAula = {"integerValue": estudiante.fields.codigoAula.integerValue}
			perfil.estado = {"stringValue": "PRUEBA_4_OK"}
			perfil.nombreUsuario = {"stringValue": estudiante.fields.nombreUsuario.stringValue}
			perfil.progreso = {"integerValue": 4}
			perfil.puntajePrueba1 = {"stringValue": estudiante.fields.puntajePrueba1.stringValue}
			perfil.puntajePrueba2 = {"stringValue": estudiante.fields.puntajePrueba2.stringValue}
			perfil.puntajePrueba3 = {"stringValue": estudiante.fields.puntajePrueba3.stringValue}
			perfil.puntajePrueba5 = {"stringValue": ""}
			noti.show()
			yield(get_tree().create_timer(2), "timeout")
			noti.text = "Guardando..."
			yield(get_tree().create_timer(1), "timeout")
			FireBase.update_document("estudiantes/%s" % FireBase.user_info.id, perfil, http)
			yield(get_tree().create_timer(2), "timeout")
			noti.hide()
			yield(get_tree().create_timer(1), "timeout")
			post_reto()
		else:
			print(str("INCORRECTO v1"))
			perfil.puntajePrueba4 = {"stringValue": "INCORRECTO"}
			noti.show()
			noti.text = "Respuesta incorrecta."
	

func post_reto():
	yield(get_tree().create_timer(3), "timeout")
	cap5_v6()

func cap5_v6():
	NovelBuilding._counter_showup(0,255,1,juan_primer,0.5, "next_1")
	NovelBuilding._dialog_v1("JUAN: \n¡Menos mal que tenía esa fórmula! \nY gracias por ayudarme. En cuanto a Yuli...",
		20,535,530, 5,1.5, dialog_bg,dialog_lb)

func next_1():
	NovelBuilding._dialog_v1("JUAN: \nMe tocará trabajar el doble, si es que quiero \ncontinuar con el plan de entrar a la universidad.",
		20,535,530, 5,1.5, dialog_bg,dialog_lb)
	NovelBuilding.interac="next_2"

func next_2():
	yield(get_tree().create_timer(0.5), "timeout")
	get_tree().change_scene("res://vista/cap_6.tscn")

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	estudiante = result_body
	match response_code:
		404:
			print("400")
			return
		200:
			print(str("Cargando... "))

func _input(event):	
	if event is InputEventMouseButton:
		print(str("POS: ", event.position.x,", ", event.position.y))
		print(str("interact: ", NovelBuilding.interac))
		if NovelBuilding.interac == "comment_1_v1" and event.pressed:
			comment_1_v1()
		if NovelBuilding.interac == "comment_1_v2" and event.pressed:
			comment_1_v2()
		if NovelBuilding.interac == "cap5_v2" and event.pressed:
			cap5_v2()
		if NovelBuilding.interac == "_interact_1" and event.pressed:
			_interact_1()
		if NovelBuilding.interac == "_interact_1_result" and event.pressed and event.position.x >= 0 and event.position.x <= 400 and event.position.y >= 112 and event.position.y <= 370:
			_interact_1_result_foward()
		if NovelBuilding.interac == "_interact_1_result" and event.pressed and event.position.x >= 0 and event.position.x <= 400 and event.position.y >= 432 and event.position.y <= 620:
			_interact_1_result_wrong()
		if NovelBuilding.interac == "cap5_v3" and event.pressed:
			cap5_v3()
		if NovelBuilding.interac == "comment_2_v1" and event.pressed:
			comment_2_v1()
		if NovelBuilding.interac == "comment_2_v2" and event.pressed:
			comment_2_v2()
		if NovelBuilding.interac == "comment_2_v3" and event.pressed:
			comment_2_v3()
		if NovelBuilding.interac == "comment_2_v4" and event.pressed:
			comment_2_v4()
		if NovelBuilding.interac == "comment_2_v5" and event.pressed:
			comment_2_v5()
		if NovelBuilding.interac == "cap5_v4" and event.pressed:
			cap5_v4()
		if NovelBuilding.interac == "cap5_v5" and event.pressed:
			cap5_v5()
		if NovelBuilding.interac == "reto_" and event.pressed:
			reto_()
		if NovelBuilding.interac == "next_1" and event.pressed:
			next_1()
		if NovelBuilding.interac == "next_2" and event.pressed:
			next_2()
