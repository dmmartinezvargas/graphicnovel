extends Node2D

onready var http: HTTPRequest = $HTTPRequest

onready var correo: LineEdit = $line_Correo
onready var contrasenia: LineEdit = $line_Contrasenia
onready var confContrasenia: LineEdit = $line_ConfContrasenia

onready var notification: Label = $notification

func _on_Atras_pressed():
	get_tree().change_scene("res://Login.tscn")

func _on_HTTPRequest_request_completed(result:int, response_code:int, headers: PoolStringArray, body:PoolByteArray):
	var response_body: = JSON.parse(body.get_string_from_ascii())
	if response_code != 200:
		notification.text = response_body.result.error.message.capitalize()
		print(str(response_body.result.error.message.capitalize()))
		if(response_body.result.error.message.capitalize() == "Invalid Email"):
			notification.text ="Email no válido."
		if(response_body.result.error.message.capitalize() == "Weak Password : Password Should Be At Least 6 Characters"):
			notification.text ="Contraseña débil. \nDebe contener al menos 6 caracteres."
	else:
		#Cuando el código es 200 (Accso correcto al servidor), es deicr, el método del botón que llama una instancia de Firebase,
		#es correcta, entonces accede a la Autenticación de Firebase y guarda los datos
		notification.text = "Registro exitoso"
		yield(get_tree().create_timer(2.0), "timeout")
		get_tree().change_scene("res://Login.tscn")

func _on_Registrarse_pressed():
	#Al presionar el botón, y las validación filtran correctamente, usa el registro (POST) de Firebase para crear un usuario docente.
	if contrasenia.text != confContrasenia.text or correo.text.empty() or contrasenia.text.empty():
		notification.text = "Contraseña o correo inválido."
		return
	FireBase.register(correo.text, contrasenia.text, http)
