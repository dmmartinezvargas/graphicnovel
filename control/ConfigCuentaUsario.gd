extends Node2D

onready var http: HTTPRequest = $HTTPRequest
#Campo a llenar por el usuario, para actualizar su nombre sin definir en la base de datos.
onready var nombreProfeEdit: LineEdit = $nombreProfeEdit
#Label para darle notificaciones al usuario, inicia vació y solo toma valores cuando el servidor trae información.
onready var notificacion: Label = $notificacion

var informacion_enviada := false
#Dato llenado por el usuario (docente)
var perfil :={
	"nombreUsuario": {},
}

#Cuando la aplicación inicia, la instancia de FireBase inmediatamente trae (método GET), la información del usuari que ha iniciado sesión (docente).
func _ready() -> void:
	FireBase.get_document("docentes%s" % FireBase.user_info.id, http)

func _on_HTTPRequest_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary	
	match response_code:
		404:
			print("400")
			notificacion.text = "Por favor, ingresa la información."
			informacion_enviada = true
			return
		200:
			print("200")
			if informacion_enviada:
				notificacion.text = "Nombre actualizado!"
				informacion_enviada = false
#Cuando se presiona el botón, activa esta función.
func _on_cambiarNombre_pressed():
	if nombreProfeEdit.text.empty():
		notificacion.text = "Por favor, ingresa un nombre para editar"
		return
	#El diccionario perfil, recibe el valor ingresado por el usuario para que lo guarde.
	perfil.nombreUsuario = {"stringValue": nombreProfeEdit.text}
	print("intento")
	#La instnacia FireBase, usa el método update (PATCH), con la información a modificar.
	FireBase.update_document("docentes/%s" % FireBase.user_info.id, perfil, http)	
	informacion_enviada = true

func _on_Atras_pressed():
	get_tree().change_scene("res://vista/aulaconfig.tscn")

