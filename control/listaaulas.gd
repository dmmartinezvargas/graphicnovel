extends Node2D
#En esta vista, el docente logeado, podrá generar 2 archivos .csv que representa el reporte de alumnos y aulas.

onready var http: HTTPRequest = $HTTPRequest
onready var notificacion: Label = $notificacion

var httpc = HTTPClient.new() # Create the Client.

#Variable para guardar la id del docente de la base de datos para mejor manejo
var docenteId = 0
#Arreglo de los datos de las aulas, traídos directamente de la base de datos, sin arreglar para ser mostrados.
var aulas_: = []
#Arreglo de datos de los estudiantes, traídos directamente de la base de datos, sin arreglar para ser mostrados.
var estudiantesDeAulasDocenteActual = []
#En el transcurso de este script, esta variable se traduce como todas las aula que pertenecen al docente logeado.
var aulaDeDocenteActual = []
#En el transcurso de este script, esta variable se traduce como todos los estudiantes que pertenecen
#a un aula del docente
var listaEstudiante = []

var endAula: = false


func _ready() -> void:
	#Al momento de iniciar esta vista, consulta todas las aulas registradas en la base de datos.
	FireBase.get_document("aulas", http) #Aquí inicia el @Paso 1

func _on_HTTPRequest_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	match response_code:
		404:
			print("400")
			return
		200:
			print("200")
			#Si el acceso de la base datos tiene datos adentro del JSON resultando,
			#entonces se almacena la id del docente de la base de datos en la variable docenteId
			if len(result_body) >= 1:
				docenteId = FireBase.user_info.id
				#@Paso 1
				#En este paso, activado por el botón actualizar y aplicado el respecivo método _on_actualizar_pressed(),
				#capturan los datos de todas las aulas, y se agregan al arreglo aulas_
				for x in len(result_body.documents):
					aulas_.append(result_body.documents[x])
			#Boolena para controlar la secuencia de pasos, si ya se terminó la consulta de aulas,
			#Cambia su valor para seguir al paso de consultar todos los estudiantes.
			endAula = true
			
			#@Paso 2
			if endAula==true && len(aulaDeDocenteActual) >= 1 && len(result_body) >= 1:
				#Cuando se termine de consultar las aulas, sigue consultar los estudiantes
				#Esto se guarda en el arreglo estudiantesDeAulasDocenteActual.
				for x in len(result_body.documents):
					estudiantesDeAulasDocenteActual.append(result_body.documents[x].fields)
				#Cuando este arreglo se llene de todos los estudiantes, se aplica el método _estudiantes()
				#Dicho método consultará las aulas del docente logeado, y compara los estudiantes que
				#tengan el mismo código, y los agrega al reporte.
				_estudiantes()

func _on_actualizar_pressed():
	#Al momento de presionar el botón Actualizar,
	#se utiliza los métodos de manejo de archivos inmersos en Godot,
	#para hacer el archivo que será el reporte .csv
	#se usa open para declarar donde se creará o dónde está (Si ya está creado) y,
	#una variable interna (en este caso WRITE, para expresar que será para escribir/editarlo)
	var archivoReporte = File.new()	
	archivoReporte.open("res://data/data_aulas.csv", File.WRITE)
	
	#Aquí se comparará las id del docente logeado, con la id del docente que se registra al crear un aula.
	#Si es igual, se entiende que esa aula es creada por él.
	#Cuando un aula resulta ser del docente, se agrega al arreglo de aulas que le pertenecen al docente,
	#que se proyectaen la variable global aulaDeDocenteActual
	for x in len(aulas_):
		var docenteIdEnDb = aulas_[x].fields.docenteId.stringValue
		if(docenteIdEnDb == FireBase.user_info.id):
			var aulaInfo = [str("Código de acceso: ",aulas_[x].fields.codigoAcceso.integerValue) , str("Nombre aula: ",aulas_[x].fields.nombreaula.stringValue)]
			aulaDeDocenteActual.append(aulaInfo)
	
	#Aquí inicia el @Paso 2 que re-activa el método escucha las peticiones HTTP
	#llamado: _on_HTTPRequest_request_completed
	#Se llaman a los estudiantes para generar el segundo reporte, que corresponde a los estudiantes,
	#que hacen parte de las aulas captadas que son creadas por el docente.
	FireBase.get_document("estudiantes",http)
	print(str("aulas para segunda vuelta: ", aulaDeDocenteActual, ", tamaño: ", len(aulaDeDocenteActual)))
	
	#Aquí se guarda (por el método store_csv_line() ) la información en el archivo .csv que se creará.
	#se recorren el arreglo de aula, y cada aula, será una fila en el archivo
	for x in len(aulaDeDocenteActual):
		archivoReporte.store_csv_line(aulaDeDocenteActual[x], ",")
	#Así como se abre, se debe cerrar el proceso con el archivo, por los métodos open() y close()
	archivoReporte.close()

#Este método se inicia cuando ya ha comenzado el @Paso 2 en la parte final del método _on_HTTPRequest_request_completed
#Aquí se toma el arreglo que representa las aulas del docente logeado, llamado aulaDeDocenteActual
#Aquí se recibe la variable estudiantesDeAulasDocenteActual que previamente se iguala a el total de estudiantes en la apñicación
func _estudiantes():
	for x in len(aulaDeDocenteActual):
		#Variable local que representa el código de acceso de cada aula,
		#esta variable servirá para compararse con los código de aula que cada arreglo de estudiante tiene inmerso
		var cod = aulaDeDocenteActual[x][0]
		for y in len(estudiantesDeAulasDocenteActual):
			#La estructura de FireBase requiere que lo que se consulta, deba ser llamado de la forma:
			#estudiantesDeAulasDocenteActual[y].codigoAula.integerValue
			#que sería:
			#estudiantesDeAulasDocenteActual[y] -> arreglo en la posición y, es un solo estudiante.
			#codigoAula -> El atributo código de acceso.
			#FireBase exite que cada atributo se una especie de diccionario, donde según su valor,
			#se especifique en un formato: "llave":valor.
			#es decir, sea string o integer, respectivamente, sería así: {"stringValue":"valor"}, {"integerValue":90}
			if(cod == estudiantesDeAulasDocenteActual[y].codigoAula.integerValue):
				print(str("Cod aula vs cod en estudiante: ", cod, " vs ", estudiantesDeAulasDocenteActual[y].codigoAula.integerValue))
				listaEstudiante.append(estudiantesDeAulasDocenteActual[y])
				print(str("lista estudiantes: ", listaEstudiante))
	
	#Variable temporal para representar a cada estudiante(un arreglo para cada estudiante) de listaEstudiante
	var estudianteParticular = []
	#Aquí se guardarán todos los arreglos de cada estudiante (cada entreda es un arreglo (estudianteParticular = [])
	var grupoEstudiantesExportar = []

	for z in len(listaEstudiante):
		#Aquí cada atributo del estudiante se acomoda en entredas distintas del arreglo,
		#el método str() transforma todos los valores a tipo String,
		#esto es así debido a que el método store_csv_line solo recibe arreglos de string
		#De esta manera, se podrá mostrar cada dato de cada estudiante que pertenezca a un aula del docente logeado.
		#estudianteParticular.append(str(listaEstudiante[z].puntajePrueba1.stringValue))
		#estudianteParticular.append(str(listaEstudiante[z].puntajePrueba2.stringValue))
		#estudianteParticular.append(str(listaEstudiante[z].puntajePrueba3.stringValue))
		#estudianteParticular.append(str(listaEstudiante[z].puntajePrueba4.stringValue))
		#estudianteParticular.append(str(listaEstudiante[z].puntajePrueba5.stringValue))
		estudianteParticular.append(str("Nombre estudiante: ",listaEstudiante[z].nombreUsuario.stringValue))
		estudianteParticular.append(str("Estado de la novela: ",listaEstudiante[z].estado.stringValue))
		#En cada recorrido se agrega el estudiante resultando al arreglo grupoEstudiantesExportar,
		#Este arreglo será el final, con el formato adecuando y datos adecuandos para la finalidad de este script.
		grupoEstudiantesExportar.append(estudianteParticular)
	#print(str("RESULTADO FINAL: ", grupoEstudiantesExportar))
	var archivoReporte = File.new()
	
	archivoReporte.open("res://data/data_estudiantes.csv", File.WRITE)
	for y in len(grupoEstudiantesExportar):
		archivoReporte.store_csv_line(grupoEstudiantesExportar[0], ",")
	archivoReporte.close()
	
	#---------------------------------------------
	#tgnovel.v1@gmail.com
	#to: maurtz.nm@gmail.com
	#aula temp 22 16 69
	#Reconstitucion4
	yield(get_tree().create_timer(1), "timeout")
	var mail = FireBase.user_info.email
	print(str("email: ", FireBase.user_info.email))
	var file = File.new()
	file.open("getter.txt", File.WRITE)
	file.store_string(mail)
	file.close()
	
	yield(get_tree().create_timer(1), "timeout")
	#OS.execute("python3", ["sendpyv2.py"], false)
	OS.execute("pyth/python.exe", ["sendpyv2.py"], false)

	#---------------------------------------------
	notificacion.text = "Archivo generado."

func _get_request_headers() -> PoolStringArray:
	return PoolStringArray([
		"Content-Type: application/json"
	])

func _on_atras_pressed():
	get_tree().change_scene("res://vista/aulaconfig.tscn")


func _on_lista_interna_pressed():
	get_tree().change_scene("res://vista/lista_interna.tscn")
