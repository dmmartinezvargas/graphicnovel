extends Node2D

onready var http: HTTPRequest = $HTTPRequest
onready var aula: RichTextLabel = $aulas
onready var estudiantes: RichTextLabel = $estudiantes


var docenteId = 0

var aulaDeDocenteActual = []
var aulas_: = []
var estudiantesDeAulasDocenteActual = []
var listaEstudiante = []
var exportAula = []
var exportEstudiante = []

var checkAula = true
var checkEstudiantes = false
var checkEstudiantesSearch = true

func _ready():
	
	FireBase.get_document("aulas", http)

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	match response_code:
		404:
			print("400")
			return
		200:
			print("200")
			if (len(result_body) >= 1 && checkAula == true):
				print(str("asking for aulas"))
				docenteId = FireBase.user_info.id
				print(str("insideAulas"))
				for x in len(result_body.documents):
					aulas_.append(result_body.documents[x])
					
				checkAula = false
				supuestoBoton()
				for x in len(aulaDeDocenteActual):
					aula.text += str("\n", aulaDeDocenteActual[x])
			

			
			if checkEstudiantes == true && len(aulaDeDocenteActual) >= 1 && len(result_body) >= 1:
				var hasFields = []
				for docs in len(result_body.documents):
					for fie in result_body.documents[docs]:
						if (fie == "fields"):
							print(str("has fields"))
							hasFields.append(result_body.documents[docs])

				estudiantesDeAulasDocenteActual= hasFields
				
				#for x in len(result_body.documents):
					#estudiantesDeAulasDocenteActual.append(result_body.documents[x].fields)
				_estudiantes()
				print("export num: ", exportEstudiante)
				for x in len(exportEstudiante):
					
					estudiantes.text+= str("\n", exportEstudiante[x])
				
				checkEstudiantesSearch = false
				checkEstudiantes=false
				
			if(checkEstudiantesSearch == true):
				print("asking for estudents")
				FireBase.get_document("estudiantes",http)
				checkEstudiantes = true
			
			

func supuestoBoton():
	var arrAula = []
	for x in len(aulas_):
		var d = aulas_[x].fields
		for y in d:
			if(y == "docenteId"):
				arrAula.append(aulas_[x])
	for x in len(arrAula):
		var docenteIdEnDb = arrAula[x].fields.docenteId.stringValue
		if(docenteIdEnDb == FireBase.user_info.id):
			var aulaInfo = [str("Código de acceso: ",arrAula[x].fields.codigoAcceso.integerValue) , str("Nombre aula: ",aulas_[x].fields.nombreaula.stringValue)]
			aulaDeDocenteActual = aulaInfo

	#FireBase.get_document("estudiantes",http)
	print(str("aulas para segunda vuelta: ", aulaDeDocenteActual, ", tamaño: ", len(aulaDeDocenteActual)))

func _estudiantes():
	var precod = aulaDeDocenteActual[0].split(": ")
	var cod = precod[1]
	for y in len(estudiantesDeAulasDocenteActual):
		print(str("cod: ", cod ,"vs", " cod 2: ", estudiantesDeAulasDocenteActual[y].fields.codigoAula.integerValue))
		if(cod == estudiantesDeAulasDocenteActual[y].fields.codigoAula.integerValue):
			listaEstudiante.append(estudiantesDeAulasDocenteActual[y].fields)

	for z in len(listaEstudiante):
		
		exportEstudiante.append(str("Nombre estudiante: ","\n",listaEstudiante[z].nombreUsuario.stringValue,"\n","Código aula: ",listaEstudiante[z].codigoAula.integerValue,"\n","Estado de la novela: ","\n",listaEstudiante[z].estado.stringValue,"\n\n"))


func _on_atras_pressed():
	get_tree().change_scene("res://vista/listaaulas.tscn")
