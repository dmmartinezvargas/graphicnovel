extends Node2D

onready var http: HTTPRequest = $HTTPRequest
onready var intro:Sprite = $inicio
onready var sara_puesto:Sprite = $sara_puesto
onready var juan_yuli:Sprite = $juan_yuli
onready var sara_perro:Sprite = $sara_perro
onready var yuli_primer:Sprite = $yuli_primer
onready var sara_primer:Sprite = $sara_primer
onready var sara_primer_2:Sprite = $sara_primer_2
onready var yuli_juan:Sprite = $yuli_juan
onready var yuli_juan_2:Sprite = $yuli_juan_2
onready var sara_perro_risa:Sprite = $sara_perro_risa
onready var fin_yuli_juan:Sprite = $fin_yuli_juan
onready var tiempo_sol:Sprite = $tiempo_sol

onready var dialog_lb:Label = $dialog_lb
onready var dialog_bg:Sprite = $dialog_bg
onready var fin:Label = $fin
onready var results:Label = $results

onready var test_1_l:Button = $left
onready var test_1_r:Button = $right
onready var test_1_ace:Button = $accept

onready var end:Button = $end

onready var bg_:Sprite = $bg
onready var noti:Label = $noti
onready var test_1_bg_v1:Sprite = $test_v1
onready var test_1_bg_v2:Sprite = $test_v2
onready var enun:Label = $enun
onready var test_time:Label = $test_time

onready var sound_ladrido:AudioStreamPlayer2D = $ladrido

var test_1_current_ans:int = 1
var right_ans:int = 0

var result_control=false

var test_1_:bool = false
var estudiante

var perfil :={
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http) #Aquí inicia el @Paso 1
	yield(get_tree().create_timer(4), "timeout")
	cap_6v1()

func cap_6v1():
	NovelBuilding._counter_showup(0,255,1,intro,0, "")
	NovelBuilding._counter_showup(0,255,1,sara_puesto,9, "cap6_v2")
	NovelBuilding._dialog_v1("SARA: \nAquí me parece bien, estamos a la vista de todos.",
		20,25,20, 5,14, dialog_bg,dialog_lb)

func cap6_v2():
	NovelBuilding._counter_showup(0,255,1,juan_yuli,1, "cap6_v3")
	NovelBuilding._dialog_v1("YULIANA: \nJuan, ¿Me puedes ayudar con la tarea de \nmatemáticas?.",
		20,25,20, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="cap6_v3"

func cap6_v3():
	NovelBuilding._dialog_v1("JUAN: \nDesde luego. A ver...",
		20,25,20, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="reto_"

func reto_():
	
	dialog_bg.hide()
	dialog_lb.hide()
	print(str("TEST 6"))
	NovelBuilding._counter_showup(0,255,1,bg_, 1, "TEST1v2")
	yield(get_tree().create_timer(2), "timeout")
	enun.text ="Yuliana está pensativa sobre el colegio, \nrecordó su tarea pendiente:"
	enun.show()
	test_1_ = true;
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding.endLoop = false
	NovelBuilding.change_v = 1
	test_1_current_ans = 1
	reto_v2()

func reto_v2():
	yield(get_tree().create_timer(2), "timeout")
	juan_yuli.hide()
	if(test_1_):
		test_1_l.visible = true
		test_1_r.visible = true
		test_1_ace.visible = true
	
	NovelBuilding.test_time_counter(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2, right_ans)
	NovelBuilding.testLooping(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2, right_ans)
	
	test_time.show()

func _on_left_pressed():
	if(test_1_):
		test_1_current_ans+=1
		if (test_1_current_ans>=5):
			test_1_current_ans=1
	fixing()

func _on_right_pressed():
	if(test_1_):
		test_1_current_ans-=1
		if (test_1_current_ans<=0):
			test_1_current_ans=4
	fixing()

func fixing():
	
	if(NovelBuilding.change_v == 1):
		right_ans=1
		moving(test_1_bg_v1)
	if(NovelBuilding.change_v == 2):
		right_ans=2
		moving(test_1_bg_v2)

func moving(sp:Sprite):
	print(str("current: ",test_1_current_ans))
	if(test_1_current_ans==1):
		sp.position.x = 0
	if(test_1_current_ans==2):
		sp.position.x = -400
	if(test_1_current_ans==3):
		sp.position.x = -800
	if(test_1_current_ans==4):
		sp.position.x = -1200

func _on_accept_pressed():
	if(test_1_):
		print(str("ANSWER: ", test_1_current_ans))
		#Para v1, respuesta es 1
		if(test_1_current_ans == right_ans):
			NovelBuilding.endLoop = true
			noti.text = "Respuesta correcta."
			print(str("CORRECTO v1; salir del loop: ", NovelBuilding.endLoop))
			test_time.hide()
			perfil.puntajePrueba5 = {"stringValue": "CORRECTO"}
			perfil.codigoAula = {"integerValue": estudiante.fields.codigoAula.integerValue}
			perfil.estado = {"stringValue": "PRUEBA_3_OK"}
			perfil.nombreUsuario = {"stringValue": estudiante.fields.nombreUsuario.stringValue}
			perfil.progreso = {"integerValue": 5}
			perfil.puntajePrueba1 = {"stringValue": estudiante.fields.puntajePrueba1.stringValue}
			perfil.puntajePrueba2 = {"stringValue": estudiante.fields.puntajePrueba2.stringValue}
			perfil.puntajePrueba3 = {"stringValue": estudiante.fields.puntajePrueba2.stringValue}
			perfil.puntajePrueba4 = {"stringValue": estudiante.fields.puntajePrueba2.stringValue}
			noti.show()
			yield(get_tree().create_timer(2), "timeout")
			noti.text = "Guardando..."
			yield(get_tree().create_timer(1), "timeout")
			FireBase.update_document("estudiantes/%s" % FireBase.user_info.id, perfil, http)
			yield(get_tree().create_timer(2), "timeout")
			noti.hide()
			yield(get_tree().create_timer(1), "timeout")
			post_reto()
		else:
			print(str("INCORRECTO v1"))
			perfil.puntajePrueba5 = {"stringValue": "INCORRECTO"}
			noti.show()
			noti.text = "Respuesta incorrecta."
			


func post_reto():
	NovelBuilding._counter_showup(0,255,1,juan_yuli,1, "comment_1_v1")
	NovelBuilding._dialog_v1("JUAN: \nCuándo te sientas mal me avisas y nos vamos.",
		20,25,20, 5,1, dialog_bg,dialog_lb)

func comment_1_v1():
	NovelBuilding._dialog_v1("YULIANA: \n¡Vamos Juan! Apenas tengo un mes, no voy a parir \nhoy aquí.",
		20,25,20, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v3"

func comment_1_v3():
	NovelBuilding._counter_showup(0,255,1,sara_perro,1, "comment_1_v4")
	yield(get_tree().create_timer(5.5), "timeout")
	sound_ladrido.play()
	

func comment_1_v4():
	dialog_bg.hide()
	dialog_lb.hide()
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding._dialog_v1("SARA: \nDisculpen, cree que todos quieren jugar con él.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v5"

func comment_1_v5():
	NovelBuilding._counter_showup(0,255,1,yuli_juan,0.5, "comment_1_v6")
	NovelBuilding._dialog_v1("JUAN: \nTranquila, no pasa nada ¿Cómo se llama?",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")

func comment_1_v6():
	NovelBuilding._dialog_v1("YULIANA: \n¿Te puedes llevar a tu mascota de aquí?",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v7"

func comment_1_v7():
	NovelBuilding._dialog_v1("SARA: \nSe llama Kaiser, y sí, me disculpo de nuevo.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding._counter_showup(0,255,1,sara_primer,1.2, "comment_1_v8")

func comment_1_v8():
	NovelBuilding._counter_showup(0,255,1,yuli_primer,1.5, "comment_1_v9")
	NovelBuilding._dialog_v1("YULIANA: \nJuan, mira esos vasos de salpicón, vamos a \ncomprar uno que tengo la garganta seca.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")

func comment_1_v9():
	NovelBuilding._counter_showup(0,255,1,sara_primer_2,0.5, "comment_1_v10")
	NovelBuilding._dialog_v1("SARA: \nNo se molesten, yo invito, soy la vendedora.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)

func comment_1_v10():
	NovelBuilding._counter_showup(0,255,1,yuli_juan_2,0.5, "comment_1_v11")
	NovelBuilding._dialog_v1("YULIANA: \nSí esta es tu manera de disculparte, me dejo \narrancar los brazos de tu perro.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)

func comment_1_v11():
	NovelBuilding._dialog_v1("JUAN: \nGracias, pero prefiero pagarte. Por cierto, me \nllamo Juan.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v12"

func comment_1_v12():
	NovelBuilding._dialog_v1("YULIANA: \nY yo, Yuliana.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v13"

func comment_1_v13():
	NovelBuilding._counter_showup(0,255,1,sara_perro_risa,0.5, "comment_1_v14")
	NovelBuilding._dialog_v1("SARA: \n¿Son hermanos?",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding.interac="comment_1_v14"

func comment_1_v14():
	NovelBuilding._counter_showup(0,255,1,fin_yuli_juan,0.5, "comment_1_v15")
	NovelBuilding._dialog_v1("YULIANA: \nSí, ¿no ves el parecido?",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v15"

func comment_1_v15():
	NovelBuilding._dialog_v1("SARA: \nMucho gusto, soy Sara.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(3), "timeout")
	NovelBuilding.interac="fin_"

func fin_():
	NovelBuilding._counter_showup(0,255,1,bg_,0.5, "fin_v")
	yield(get_tree().create_timer(1), "timeout")
	fin.show()
	dialog_bg.hide()
	dialog_lb.hide()
	end.show()

func fin_v():
	#result_control = true
	yield(get_tree().create_timer(1), "timeout")
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http)


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	estudiante = result_body
	match response_code:
		404:
			print("400")
			return
		200:
			print(str("Cargando... "))
			
			if(result_control):
				print(str("es independiente?"))
				print(str(result_body))
				
				var estudiantes = result_body
				for x in result_body.fields:
					if(x == "tipo"):
						print(str("shi"))
						var insideId = FireBase.user_info.id
						print(str("inside:",insideId))
						var Allresult = result_body.name.split("/")
						var outsideId = Allresult[6]
						print(str("outside:",outsideId))
						if(insideId == outsideId):
							var v1 = result_body.fields.puntajePrueba1.stringValue
							var v2 = result_body.fields.puntajePrueba2.stringValue
							var v3 = result_body.fields.puntajePrueba3.stringValue
							var v4 = result_body.fields.puntajePrueba4.stringValue
							var v5 = result_body.fields.puntajePrueba5.stringValue
							
							#results.show()
							#results.text = "Respuestas \n\n"+"1. "+v1+"\n"+"2. "+v2+"\n"+"3. "+v3+"\n"+"4. "+v4+"\n"+"5. "+v5
					else:
						print("pos ño")

func _input(event):	
	if event is InputEventMouseButton:
		if NovelBuilding.interac == "cap6_v2" and event.pressed:
			cap6_v2()
		if NovelBuilding.interac == "cap6_v3" and event.pressed:
			cap6_v3()
		if NovelBuilding.interac == "reto_" and event.pressed:
			reto_()
		if NovelBuilding.interac == "comment_1_v1" and event.pressed:
			comment_1_v1()
		if NovelBuilding.interac == "comment_1_v3" and event.pressed:
			comment_1_v3()
		if NovelBuilding.interac == "comment_1_v4" and event.pressed:
			comment_1_v4()
		if NovelBuilding.interac == "comment_1_v5" and event.pressed:
			comment_1_v5()
		if NovelBuilding.interac == "comment_1_v6" and event.pressed:
			comment_1_v6()
		if NovelBuilding.interac == "comment_1_v7" and event.pressed:
			comment_1_v7()
		if NovelBuilding.interac == "comment_1_v8" and event.pressed:
			comment_1_v8()
		if NovelBuilding.interac == "comment_1_v9" and event.pressed:
			comment_1_v9()
		if NovelBuilding.interac == "comment_1_v10" and event.pressed:
			comment_1_v10()
		if NovelBuilding.interac == "comment_1_v11" and event.pressed:
			comment_1_v11()
		if NovelBuilding.interac == "comment_1_v12" and event.pressed:
			comment_1_v12()
		if NovelBuilding.interac == "comment_1_v13" and event.pressed:
			comment_1_v13()
		if NovelBuilding.interac == "comment_1_v14" and event.pressed:
			comment_1_v14()
		if NovelBuilding.interac == "comment_1_v15" and event.pressed:
			comment_1_v15()
		if NovelBuilding.interac == "fin_" and event.pressed:
			fin_()
		if NovelBuilding.interac == "fin_v" and event.pressed:
			fin_v()
		if NovelBuilding.interac == "post_reto" and event.pressed:
			post_reto()

func _on_end_pressed():
	get_tree().change_scene("res://vista/aulaconfig.tscn")
