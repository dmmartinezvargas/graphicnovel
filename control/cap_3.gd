extends Node2D

onready var http: HTTPRequest = $HTTPRequest

onready var intro:Sprite = $inicio
onready var maleta_juntos:Sprite = $maleta_juntos
onready var yuli_1:Sprite = $yuli_1
onready var descri_yuli:Sprite = $descri_yuli
onready var llamada_brayan:Sprite = $llamada_brayan
onready var colgar:Sprite = $colgar
onready var farmacia:Sprite = $farmacia
onready var tiempo_sol:Sprite = $tiempo_sol

onready var dialog_lb:Label = $dialog_lb
onready var dialog_bg:Sprite = $dialog_bg

onready var test_1_l:Button = $left
onready var test_1_r:Button = $right
onready var test_1_ace:Button = $accept
onready var bg_:Sprite = $bg_
onready var noti:Label = $noti

onready var test_1_bg_v1:Sprite = $test_v1
onready var test_1_bg_v2:Sprite = $test_v2
onready var enun:Label = $enun
onready var test_time:Label = $test_time

var test_1_current_ans:int = 1
var right_ans:int = 0

var estudiante
var state_a=false

var test_1_:bool = false

var perfil :={
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http) #Aquí inicia el @Paso 1
	yield(get_tree().create_timer(4), "timeout")
	cap_3v1()

func cap_3v1():
	NovelBuilding._counter_showup(0,255,1,intro,0, "")
	NovelBuilding._counter_showup(0,255,1,maleta_juntos,9, "cap3.0")
	NovelBuilding._dialog_v1("JUAN: \nAhora vuelvo, voy a pagar los recibos.",
		20,515,510, 5,13, dialog_bg,dialog_lb)

func cap_3v1v1():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,yuli_1,1, "cap3.1")

func interac_1():
	descri_yuli.show()
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="cap3.2"

func cap_3v2():
	NovelBuilding._counter_showup(0,255,1,llamada_brayan,1, "brayan_1")
	NovelBuilding._dialog_v1("YULIANA: \n¿Dónde estás? Te dije que a las dos en punto en la \ndroguería de la esquina... no pienso ir sola a \ncomprar eso, ¿quién te crees?",
		20,545,540, 5,7, dialog_bg,dialog_lb)

func comment_1v1():
	NovelBuilding._dialog_v1("BRAYAN: \nEstoy en la casa, pero dale suave, que ya voy para \nallá... me duele un pie.",
		20,545,540, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="colgarv1"

func colgar():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,colgar,1, "colgarv2")

func farmacia():
	NovelBuilding._counter_showup(0,255,1,farmacia,1, "reto_")
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding._dialog_v1("YULIANA: \nTendré que comprala yo misma...",
		20,545,540, 5,1, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1.5), "timeout")
	NovelBuilding.interac="reto_"

func reto_():
	dialog_bg.hide()
	dialog_lb.hide()
	print(str("TEST 3"))
	NovelBuilding._counter_showup(0,255,1,bg_, 1, "TEST1v2")
	yield(get_tree().create_timer(2), "timeout")
	yield(get_tree().create_timer(2), "timeout")
	enun.text ="Yuliana no sabe si le alcanzará el \ndinero..."
	enun.show()
	test_1_ = true;
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding.endLoop = false
	NovelBuilding.change_v = 1
	reto_v2()

func reto_v2():
	yield(get_tree().create_timer(2), "timeout")
	
	if(test_1_):
		test_1_l.visible = true
		test_1_r.visible = true
		test_1_ace.visible = true
	
	NovelBuilding.test_time_counter(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2, right_ans)
	NovelBuilding.testLooping(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2, right_ans)
	
	test_time.show()
	
func _on_left_pressed():
	if(test_1_):
		test_1_current_ans+=1
		if (test_1_current_ans>=5):
			test_1_current_ans=1
	fixing()

func _on_right_pressed():
	if(test_1_):
		test_1_current_ans-=1
		if (test_1_current_ans<=0):
			test_1_current_ans=4
	fixing()

func fixing():
	
	if(NovelBuilding.change_v == 1):
		right_ans=4
		moving(test_1_bg_v1)
	if(NovelBuilding.change_v == 2):
		right_ans=3
		moving(test_1_bg_v2)

func moving(sp:Sprite):
	print(str("current: ",test_1_current_ans))
	if(test_1_current_ans==1):
		sp.position.x = 0
	if(test_1_current_ans==2):
		sp.position.x = -400
	if(test_1_current_ans==3):
		sp.position.x = -800
	if(test_1_current_ans==4):
		sp.position.x = -1200

func _on_accept_pressed():
	if(test_1_):
		print(str("ANSWER: ", test_1_current_ans))
		#Para v1, respuesta es 1
		if(test_1_current_ans == right_ans):
			NovelBuilding.endLoop = true
			noti.text = "Respuesta correcta."
			print(str("CORRECTO v1; salir del loop: ", NovelBuilding.endLoop))
			test_time.hide()
			perfil.puntajePrueba3 = {"stringValue": "CORRECTO"}
			perfil.codigoAula = {"integerValue": estudiante.fields.codigoAula.integerValue}
			perfil.estado = {"stringValue": "PRUEBA_3_OK"}
			perfil.nombreUsuario = {"stringValue": estudiante.fields.nombreUsuario.stringValue}
			perfil.progreso = {"integerValue": 3}
			perfil.puntajePrueba1 = {"stringValue": estudiante.fields.puntajePrueba1.stringValue}
			perfil.puntajePrueba2 = {"stringValue": estudiante.fields.puntajePrueba2.stringValue}
			perfil.puntajePrueba4 = {"stringValue": ""}
			perfil.puntajePrueba5 = {"stringValue": ""}
			noti.show()
			yield(get_tree().create_timer(2), "timeout")
			noti.text = "Guardando..."
			yield(get_tree().create_timer(1), "timeout")
			FireBase.update_document("estudiantes/%s" % FireBase.user_info.id, perfil, http)
			yield(get_tree().create_timer(2), "timeout")
			noti.hide()
			yield(get_tree().create_timer(1), "timeout")
			post_reto()
		else:
			print(str("INCORRECTO v1"))
			perfil.puntajePrueba3 = {"stringValue": "INCORRECTO"}
			noti.show()
			noti.text = "Respuesta incorrecta."


func post_reto():
	yield(get_tree().create_timer(1), "timeout")
	dialog_bg.hide()
	dialog_lb.hide()
	yield(get_tree().create_timer(1), "timeout")
	get_tree().change_scene("res://vista/cap_4.tscn")

func _input(event):
	if event is InputEventMouseButton:
		#print(str("interact: ", NovelBuilding.interac))
		if NovelBuilding.interac == "cap3.0" and event.pressed:
			cap_3v1v1()
		if NovelBuilding.interac == "cap3.1" and event.pressed:
			interac_1()
		if NovelBuilding.interac == "cap3.2" and event.pressed:
			cap_3v2()
		if NovelBuilding.interac == "brayan_1" and event.pressed:
			comment_1v1()
		if NovelBuilding.interac == "colgarv1" and event.pressed:
			colgar()
		if NovelBuilding.interac == "colgarv2" and event.pressed and event.position.x >= 270 and event.position.x <= 340 and event.position.y >= 430 and event.position.y <= 492:
			farmacia()
		if NovelBuilding.interac == "reto_" and event.pressed:
			reto_()

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	estudiante = result_body
	match response_code:
		404:
			print("400")
			return
		200:
			print(str("Cargando... "))
