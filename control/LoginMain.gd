extends Node2D

onready var http: HTTPRequest = $HTTPRequest
onready var correo: LineEdit = $line_correo
onready var contrasenia: LineEdit = $line_contrasenia
onready var notification: Label = $notificacion
onready var loginBt: Button = $btn_iniciarSesion

func _on_btn_iniciarSesion_pressed():
	if correo.text.empty() or  contrasenia.text.empty():
		notification.text = "Por favor, ingresa correo y contraseña."
		return
	#Accede a la Autenticación de FireBase que almacena los datos registrador, y confirmación token y credenciales.
	FireBase.login(correo.text, contrasenia.text, http)

func _on_HTTPRequest_request_completed(result:int, response_code:int, headers: PoolStringArray, body: PoolByteArray):
	var response_body = JSON.parse(body.get_string_from_ascii())
	if response_code != 200:
		#Si la respuesta al servidor es negativa (400), traerá el error que FireBase indique y se lo mostrará directamente,
		#al usuario, a través de ntoficacion (label).
		print(str(response_body.result.error.message.capitalize()))
		if(response_body.result.error.message.capitalize() == "Email Not Found"):
			notification.text = "Email no encontrado."
		if(response_body.result.error.message.capitalize() == "Invalid Email"):
			notification.text = "Email no válido."
		if(response_body.result.error.message.capitalize() == "Invalid Password"):
			notification.text = "Contraseña no válida."

	else:
		notification.text = "Ingreso exitoso."
		yield(get_tree().create_timer(2.0), "timeout")
		#Si las credenciales que captura el método del botón, son correctas, pasa al a vista de configuración de aula.
		get_tree().change_scene("res://vista/aulaconfig.tscn")

func _on_Registrarse_pressed():
	get_tree().change_scene("res://vista/Register.tscn")
