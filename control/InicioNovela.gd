extends Node2D
#Esta vsita está hecha para el estudiante, para que ingrese un código y se sincronice con un aula.
onready var http: HTTPRequest = $HTTPRequest
onready var codigo: LineEdit = $codigoaula
onready var nombreEstudante: LineEdit = $nombreEstudiante
onready var notificacion: Label = $notificacion
# usar el código para detectar el profe y aula, luego, usar el path para crear un estudiante con el nomnbre y demás.
var informacion_enviada := false

var aulas:=[]

var send=0
#En este punto el estudiante solo tiene información en la parte de Autentificación de FireBase,
#Para agregarlo a Cloud Firestore, se utilizará este arreglo para darle valores al diccionario de cada estudiante en la base de datos
var estudiante:= {
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready() -> void:
	#Consulta a los profesores para luego tratar esa información para acceder a las aulas.
	FireBase.get_document("docentes%s" % FireBase.user_info.id, http)

func _on_HTTPRequest_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray):
	#Variable que acoje todo valor que recibe de las solicitudes HTTP
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	match response_code:
		404:
			print("400")
			notificacion.text = "Por favor, ingrese el código el aula."
			informacion_enviada = true
			return
			
		200:
			print("200")
			#Condicional para que cuando el servidor traiga los datos de 
			if len(result_body) >= 1 && send==1: #para cuando se consulte, que a veces entren peticiones vacías	
				send=2
				var arr = []
				#db	/ docente en la segunda posición (0) / datos del docente
				for x in len(result_body.documents):
					var printer = result_body.documents[x]
					var grupoDocenteIdFull = result_body.documents[x].fields.codigoAcceso.integerValue
					arr.append(grupoDocenteIdFull)
					notificacion.text = "Buscando..."
				full_aulas(arr)
			if informacion_enviada:
				informacion_enviada = false

func _on_Inicio_pressed():
	print(str("send botón: ",send))
	send=1
	if codigo.text.empty() or nombreEstudante.text.empty():
		notificacion.text = "Por favor, llena todos los campos."
		return
	#Crea o modifica el estudiante, según este retomando o iniciando la novela.
	estudiante.nombreUsuario = {"stringValue": nombreEstudante.text}
	estudiante.puntajePrueba1 = {"stringValue": ""}
	estudiante.puntajePrueba2 = {"stringValue": ""}
	estudiante.puntajePrueba3 = {"stringValue": ""}
	estudiante.puntajePrueba4 = {"stringValue": ""}
	estudiante.puntajePrueba5 = {"stringValue": ""}
	estudiante.progreso = {"integerValue": 0}
	estudiante.estado = {"stringValue": "INICIO"}
	
	FireBase.get_document("aulas", http)
	informacion_enviada = true
	

func _on_atras_pressed():
	get_tree().change_scene("res://vista/aulaconfig.tscn")

#Método creado al momento de presionar Inicio
#Compara y detecta la aula escrita por el usuario si existe o no, dejará comenzar o no.
#He inmediatamente sincroniza los datos de login, con los de ser un estudiantes.
#Es decir, Firebase crea un usuario para hacer login, pero estos datos son separados de 
#los que se guarda como base de datos (Cloud Firestore).
#lo que se hace aquí es que toma la ID para login y los datos que se crean aquí mismo
#para crear el perfil.
func full_aulas(grupo:Array):
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	var cod = rng.randi_range(1000000,9999999)

	if send == 2:
		aulas = grupo
		for x in len(aulas):
			print(str("comparativa: "," llega: ",int(codigo.text), ", en db: ", int(aulas[x]) ))

			if int(codigo.text) == int(aulas[x]):
				yield(get_tree().create_timer(1.0), "timeout")
				estudiante.codigoAula ={"integerValue": int(aulas[x])}
				FireBase.save_document("estudiantes?documentId=%s" % FireBase.user_info.id , estudiante, http)
				notificacion.text = "Aula encontrada. Accediendo..."
				yield(get_tree().create_timer(2), "timeout")
				get_tree().change_scene("res://vista/cap_1.tscn")
				break;
			else:
				print("No encontrado")
				notificacion.text = "Aula no encontrada."
		print("work! ", aulas, "\n")
