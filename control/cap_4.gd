extends Node

onready var http: HTTPRequest = $HTTPRequest

onready var intro:Sprite = $inicio
onready var sara_pc:Sprite = $sara_pc
onready var sara_blog:Sprite = $sara_blog
onready var sara_juan:Sprite = $sara_juan
onready var juan_pc:Sprite = $juan_pc
onready var juan_blog:Sprite = $juan_blog
onready var llamada_yuliana:Sprite = $llamada_yuliana
onready var ringing:Sprite = $ringing
onready var tiempo_sol:Sprite = $tiempo_sol

onready var dialog_lb:Label = $dialog_lb
onready var dialog_bg:Sprite = $dialog_bg

onready var noti:Label = $noti
onready var pregunta_test_1:Sprite
onready var recibo_mojado: Sprite = $recibo_mojado
onready var bg2: Sprite = $bg2

onready var sound_llamada:AudioStreamPlayer2D = $llamada

var test_1_:bool = false
var right_ans:int = 0
var test_1_current_ans:int = 1
var estudiante

var perfil :={
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http) #Aquí inicia el @Paso 1
	yield(get_tree().create_timer(4), "timeout")
	cap_4v1()

func cap_4v1():
	NovelBuilding._counter_showup(0,255,1,intro,0, "")
	NovelBuilding._counter_showup(0,255,1,sara_pc,9, "comment_1_v1")
	yield(get_tree().create_timer(13.5), "timeout")
	NovelBuilding._dialog_v1("SARA: \nNo puedo creer lo egoísta que fui con mi madre \ny mi abuela, me lo han dado todo. No tengo por qué \nexigirles más.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)

func comment_1_v1():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,sara_blog,1, "cap3_v2")

func cap3_v2():
	NovelBuilding._counter_showup(0,255,1,sara_juan,1, "cap3_v3")

func cap3_v3():
	NovelBuilding._counter_showup(0,255,1,juan_pc,1, "cap3_v4")
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding._dialog_v1("JUAN: \nSi no fuera por estos blogs, creo que me ahogaría \ncon mis palabras...",
		20,525,520, 5,1.5, dialog_bg,dialog_lb)
	
func cap3_v4():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,juan_blog,1, "cap3_v5")

func cap3_v5():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding.stop_2 = false
	NovelBuilding._counter_showup(0,255,1,ringing,1, "")
	NovelBuilding.blink_op(ringing)
	NovelBuilding._counter_showup(0,255,1,llamada_yuliana,1.1, "comment_3_v1")
	yield(get_tree().create_timer(2), "timeout")

func comment_3_v1():
	sound_llamada.play()
	NovelBuilding.stop_2 = true
	NovelBuilding._dialog_v1("JUAN: \n¿Dónde estás? Hace rato tenías que haber llegado.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v2"

func comment_3_v2():
	NovelBuilding._dialog_v1("YULIANA: \nJuan, tengo que decirte algo...",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v3"

func comment_3_v3():
	NovelBuilding._dialog_v1("JUAN: \n¿Qué pasó?",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v4"

func comment_3_v4():
	NovelBuilding._dialog_v1("YULIANA: \nEstoy embarazada.",
		20,25,20, 5,1.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="next_"

func next_():
	get_tree().change_scene("res://vista/cap_5.tscn")

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	estudiante = result_body
	match response_code:
		404:
			print("400")
			noti.text = "Error al guardar la información."
			return
		200:
			print(str("Cargando... "))

func _input(event):	
	if event is InputEventMouseButton:
		print(str("interact: ", NovelBuilding.interac))
		if NovelBuilding.interac == "comment_1_v1" and event.pressed:
			comment_1_v1()
		if NovelBuilding.interac == "cap3_v2" and event.pressed:
			cap3_v2()
		if NovelBuilding.interac == "cap3_v3" and event.pressed:
			cap3_v3()
		if NovelBuilding.interac == "cap3_v4" and event.pressed:
			cap3_v4()
		if NovelBuilding.interac == "cap3_v5" and event.pressed:
			cap3_v5()
		if NovelBuilding.interac == "comment_3_v1" and event.pressed and event.position.x >= 185 and event.position.x <= 245 and event.position.y >= 445 and event.position.y <= 505:
			comment_3_v1()
		if NovelBuilding.interac == "comment_3_v2" and event.pressed:
			comment_3_v2()
		if NovelBuilding.interac == "comment_3_v3" and event.pressed:
			comment_3_v3()
		if NovelBuilding.interac == "comment_3_v4" and event.pressed:
			comment_3_v4()
		if NovelBuilding.interac == "next_" and event.pressed:
			next_()
