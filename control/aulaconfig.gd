extends Node2D

#En este vista, justo después dei iniciar sesión, se podrá bien crear un aula(docente), o acceder a una (estudiante).
#También permite acceder a la configuración de la cuenta
#Finalmente, permite hacer un reporte (para profesores) de las aulas y estudiantes.

#Variable que representa el objeto en la vista que permanente escucha señales de HTTP
onready var http: HTTPRequest = $HTTPRequest

#Representaciones de campos en la vista
onready var nombreAulaEdit: LineEdit = $nombreAulaEdit
onready var codigoCreado: Label = $codigoCreado
#Esta variable representa un label vacío, hecho para tomar un valor de texto para notificar en tiempo real al usuario.
onready var notificacion: Label = $notificacion

#Variable que manejará si los datos (id que trae de la instancia de Firebase) están creado o no. Es decir,
#Si es un nuevo usuario o no.
var nuevo_usuario := false

var informacion_enviada := false

#Este perfil realmente sincroniza los datos de la autorización de FireBase, y los datos de Cloud Firestore (manejo de datos de FireBase).
#El campo NombreUsuario solo prepara el atributo del docento que podrá modificarlo después,
#ya que en este momento, inicia con el valor:"sin definir".
#podrá escribir su nombre en la Configuración de cuenta(Botón).
var perfil_ :={
	"nombreUsuario": {},
}

#Este diccionario recibirá los datos introduccidos por el usuario y algunos llenados por defecto.
var aula_:={
	"codigoAcceso":{},
	"nombreaula":{},
	"docenteId":{}
}

#la funnción _ready() en Godot, es la función que no necesita acción para ser iniciada,
#solo se debe reproducir la aplicación y estar en esta vista (aulaconfig.tscn).
func _ready() -> void:
	#En archivo modelo de Firebase, que maneja los verbos HTTP, contiene el método get (consultar)
	#En este caso, consulta los datos del docente que ha iniciado sesión.
	FireBase.get_document("docentes%s" % FireBase.user_info.id, http)

#Esta función es activada con cada petición/envío HTTP que haga la aplicación cuando este en esta vista (aulaconfig.tscn).
#Si hay información, se guardará en la variable result_body dentro del scope del método.
#Un resultado 400 significa que no se pudo comunicar con el servidor de la bd	.
#Un resultado 200 quiere decir que logró comunicarse y traer o enviar información.
func _on_HTTPRequest_request_completed(result: int, response_code: int, headers: PoolStringArray, body: PoolByteArray):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	#En ambos casos, la variable notificación toma valores para notificar el estado de la comunicación del servidor.
	match response_code:
		404:
			print("400")
			notificacion.text = "Por favor, ingresa la información."
			nuevo_usuario = true
			return
		200:
			print("200")
			if informacion_enviada:
				notificacion.text = "Aula creada correctamente!"
				informacion_enviada = false

#Función que se activa con el botón (Crear), referente al a creación de aulas.
func _on_Crear_pressed() -> void:
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	#Esta variable (cod) está destinada a crear un valor (int) random para representar el código del aula.
	#Dicho código funciona para que el estudiante pueda acceder la instancia creada por el profesor.
	var cod = rng.randi_range(100000,999999)
	print(cod)
	
	var rng_2 = RandomNumberGenerator.new()
	rng_2.randomize()
	#Esta variiable crea un valor (int) random que será usada como id único para el aula en la bd.
	var nextId = rng_2.randi_range(10000000,99999999)
	print(nextId)
	
	#Validación de que el campo de nombre aula que debe llenar el docente, no puede estar vacío cuando se presione el botón.
	if nombreAulaEdit.text.empty():
		notificacion.text = "Por favor, ingresa un nombre de aula."
		return
	
	#Aquí se usan los campos de los diccionarios de perfil (para el docente) y aula_ (para el aula que está dicendo creada),
	#para qe reciba lo que el usuario acaba de ingresar, y campos que se generar automáticamente, como el código de aula y id de aula.
	perfil_.nombreUsuario = {"stringValue": "sin definir"}
	aula_.codigoAcceso = {"integerValue": cod}
	aula_.nombreaula = {"stringValue":nombreAulaEdit.text}
	aula_.docenteId = {"stringValue":FireBase.user_info.id}
	
	#Control para si el usuario (Docente) es nuevo o no.
	match nuevo_usuario:
		true:
			FireBase.save_document("docentes?documentId=%s" % FireBase.user_info.id, perfil_, http)
		false:
			#Aquí sincroniza los datos de Autorización (id), con la id que se creará, para que sea la misma
			#Y agrega el dato NombreUsuario, que como se mencionó, no será definido aún.
			FireBase.save_document("docentes?documentId=%s" % FireBase.user_info.id, perfil_, http)
			#Hace un pausa de 2 segundos para no saturar la base datos
			yield(get_tree().create_timer(2.0), "timeout")
			#Después de la pausa, el dato de nombre aula digitado y los código de acceso y id, se agregan a una nueva colección de la base de datos.
			FireBase.save_document("aulas?documentId=%s" %nextId , aula_, http)
			codigoCreado.text = str("Código de aula: ",cod)
	informacion_enviada = true

#Funciones de esta vista que redigiren a otras escenas (vistas).
func _on_atras_pressed():
	get_tree().change_scene("res://Login.tscn")

func _on_config_pressed():
	get_tree().change_scene("res://vista/ConfigCuentaUsario.tscn")

func _on_accerderAula_pressed():
	get_tree().change_scene("res://vista/InicioNovela.tscn")

func _on_listaAulas_pressed():
	get_tree().change_scene("res://vista/listaaulas.tscn")

func _on_independiente_pressed():
	get_tree().change_scene("res://vista/independiente.tscn")
