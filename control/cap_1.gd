extends Node2D

#TODOS los capítulso tienen el mismo manejo:
#1) carga de elementos visuales (sprite, labels, botones y una instnacia http)
#2) Una seguidilla de métodos que contienen los sucesos del capítulo
#3) manejo de variable de NovelBuilding 'interac' para manejo de interacciones y cambios
#de escena
#4) Manejo de inputs, particularmente del mouse, como su posición y cuando es presionado
#5) Luego, la prubea, que consta de botones o puntos de interacción, con actualización
#en la base de datos según sus respuestas.

onready var http: HTTPRequest = $HTTPRequest
#Sprite/imágenes utilizadas en el capítulo
onready var sprite_1:Sprite = $inicio_1/img_1
onready var sprite_2:Sprite = $inicio_1/img_2
onready var icon_clima_sp:Sprite = $inicio_1/img_3
onready var icon_clima_dia:Sprite = $cap_in/clima_manana
onready var libreta_1:Sprite = $cap_in/libreta_1
onready var diag_bg:Sprite = $diag
onready var diag_bg_izquierda:Sprite = $diag_izquierda
onready var diag_bg_central:Sprite = $diag_central
onready var diag_bg_externo:Sprite = $diag_externo
onready var bg:Sprite = $bg
onready var comedor:Sprite = $cap_in/comedor
onready var dioselina_silueta:Sprite = $cap_in/dioselina_silueta
onready var dioselina:Sprite = $cap_in/dioselina_comedor
onready var dioselina_info:Sprite = $cap_in/dioselina_info
onready var juan:Sprite = $cap_in/juan_comedor
onready var yuliana:Sprite = $cap_in/yuliana_comedor
onready var bg_sala:Sprite = $cap_in/bg_sala
onready var juan_mano_folleto:Sprite = $cap_in/juan_mano_folleto
onready var juan_mano_folleto_detalle:Sprite = $cap_in/juan_mano_folleto_detalle
onready var juan_mano_recibo:Sprite = $cap_in/juan_mano_recibo

onready var prueba_diag:Label = $prueba_1
onready var dialog:Label = $dialog
onready var http_noti:Label = $http_noti

onready var test_1_bg_v1: Sprite = $test_1_bg_v1
onready var test_1_bg_v2: Sprite = $test_1_bg_v2
onready var test_1_bg_v3: Sprite = $test_1_bg_v3
onready var test_1_bg_v4: Sprite = $test_1_bg_v4
onready var test_1_bg_v5: Sprite = $test_1_bg_v5
onready var test_1_bg_v6: Sprite = $test_1_bg_v6

onready var test_time:Label = $test_time

onready var test_1_l:Button = $test_1_izq
onready var test_1_r:Button = $test_1_der
onready var test_1_ace:Button = $test_1_aceptar

onready var emergent:Label = $emergent

var right_ans:int = 0
var test_1_:bool = false
var test_1_current_ans:int = 1
var estudiante

#Variable para actualizar los datos del estudiante según progrese en el capítulo
var perfil :={
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	#Inmediatamente inicial la app, se llaman los datos del usuario.
	#Esto sirve como prueba si tiene o no internet, pero principalmente se hace
	#debido a que se usuarán los datos que ingreso al final del capítulo.
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http) #Aquí inicia el @Paso 1
	yield(get_tree().create_timer(5.5), "timeout")
	_cap_1()

#Aquí se comienzan a utilizar los métodos hechos en NovelBuilding, con las sprite cargados previamente.
func _cap_1():
	NovelBuilding._counter_showoff(255,0,1, sprite_1,"")
	NovelBuilding._movement_x_left_right(icon_clima_sp, 450,200, 1, 4.5, 35)
	NovelBuilding._counter_showup(0,255,1,sprite_2,2, "")
	NovelBuilding._counter_showup(0,255,1,icon_clima_sp, 4.5, "")
	NovelBuilding._counter_showup(0,255,1,libreta_1,10.0, "CAP1.1")
	NovelBuilding._dialog_v1("\nJUAN: \nEl colegio de Yuliana está cada vez más costoso; \npero bueno, ¡ella lo vale y se lo merece!",
	20,15,5,5,15.0, diag_bg,dialog)
	NovelBuilding._counter_showoff_2(15,sprite_2 )
	NovelBuilding._counter_showoff_2(15,icon_clima_sp )
	yield(get_tree().create_timer(16), "timeout")
	var pos:Vector2 = Vector2(20,74)
	emergent.set_position(pos)
	emergent.show()
	emergent.text="(Toca la pantalla para continuar)."
	#comedor

func cap_1_v2():
	emergent.hide()
	NovelBuilding._counter_showoff_2(0,libreta_1 )
	NovelBuilding._counter_showup(0,255,1,comedor,0, "")
	NovelBuilding._counter_showup(0,255,1,yuliana,0, "")
	NovelBuilding._counter_showup(0,255,1,dioselina_silueta,0, "")
	NovelBuilding._counter_showup(0,255,1,juan,0, "")
	NovelBuilding._counter_showoff_2(0,diag_bg)
	NovelBuilding._counter_showoff_2_lb(0,dialog)
	NovelBuilding.blink_op(dioselina_silueta)
	NovelBuilding._movement_x_left_right(icon_clima_dia, 450,200, 1, 0, 45)
	NovelBuilding._counter_showup(0,255,1,icon_clima_dia, 0, "CAP1.2")
	yield(get_tree().create_timer(1), "timeout")

func cap_1_v3():
	if (NovelBuilding.stop_1):
		NovelBuilding._counter_showup(0,255,1,dioselina,1, "CAP1.3")
		NovelBuilding._counter_showup(0,255,1,dioselina_info,0, "CAP1.3")
		print(str("seguimiento:", NovelBuilding.interac))

func conversation_1_v1():
	NovelBuilding._dialog_v1("JUAN: \nYuli, ya pagué el colegio, estás a paz y salvo. Por \ncierto, ¿cómo vas, hermanita?\n",
		20,265,255, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "CAP1.4"

func conversation_1_v2():
	NovelBuilding._dialog_v1("YULIANA: \nBien. Gracias por todo, Juan.",
		20,265,255, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "CAP1.5"

func conversation_1_v3():
	NovelBuilding._dialog_v1("DIOSELINA: \nAsí es mija, valore el esfuerzo que su hermano \nestá haciendo por usted, lo único que queda es \nel estudio.",
		20,265,255, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "CAP1.6"

func conversation_1_v4():
	NovelBuilding._dialog_v1("JUAN: \nHablando de eso, les tengo una sorpresa: voy a \nentrar a la Universidad, será difícil, pero no \nimposible.",
		20,265,255, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "CAP1.7"

func conversation_1_v5():
	NovelBuilding._dialog_v1("DIOSELINA: \n¡Qué bueno, mijo! Me entristece no ser más útil, \npero sabes que cuentas con el apoyo de Yuli y el \nmío.",
		20,265,255, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "CAP1.8"

func conversation_1_v6():
	NovelBuilding._dialog_v1("JUAN: \n¡No digas eso mamá! ¡Ya hiciste tu parte, ahora es \nel turno de nosotros!",
		20,265,255, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "CAP1.9"

func conversation_1_v7():
	NovelBuilding._counter_showoff_2(2,icon_clima_dia)
	diag_bg.hide()
	dialog.hide()
	cap_1_v4()

func cap_1_v4():
	yield(get_tree().create_timer(4.0), "timeout")
	NovelBuilding._counter_showup(0,255,1,bg_sala,0, "")
	yield(get_tree().create_timer(2.0), "timeout")
	NovelBuilding._counter_showup(0,255,1,icon_clima_dia, 0, "")
	NovelBuilding._counter_showup(0,255,1,juan_mano_folleto,0, "conversation_2_v2")
	NovelBuilding._movement_x_left_right(icon_clima_dia, 450,200, 1, 0, 45)
	yield(get_tree().create_timer(3), "timeout")
	NovelBuilding._dialog_v1("JUAN: \nTengo que sacar el tiempo para averiguar bien \nque quiero estudiar.",
		20,165,155, 5,0.5, diag_bg,dialog)
	

func conversation_2_v2():
	NovelBuilding._dialog_v1("JUAN: \nAquí hay un papel de Yuli. Parece un cuestionario \nde repaso de uno de sus cursos en el colegio.",
		20,165,155, 5,0.5, diag_bg,dialog)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac = "test_1_intro"

#Aquí comienza la prueba, test o examen. El cuál consta de una gran imagen, y cambiar la posición de esta,
#mostrado diferentes partes, dando la impresión de moverse un carrusel de imágenes.
func test_1_intro():
	diag_bg.hide()
	dialog.hide()
	print(str("TEST 1"))
	NovelBuilding._counter_showup(0,255,1,bg, 1, "TEST1v2")
	yield(get_tree().create_timer(2), "timeout")
	#prueba_diag.text="Juan está viendo un folleto de la Unicamacho y \nencuentra un “cuestionario de repaso” para un \nexamen de matemáticas que tendrá su hermana en \nel colegio. Ese cuestionario llama la atención de \nJuan y se da cuenta que, si quiere entrar a la \nuniversidad, tendrá que estudiar mucho. \nEl cuestionario comienza con la siguiente pregunta:"
	prueba_diag.text="Juan cae en cuenta que, si quiere entrar a la \nuniversidad, tendrá que estudiar mucho. \nAsí que lee el cuestionario de su hermana y se \nesfuerza en resolverlo. \n\n (Lee las preguntas, selecciona la respuesta \nque consideres sea la correcta y oprime \nel botón de ACEPTAR para continuar)."
	prueba_diag.show()
	test_1_ = true;
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding.endLoop = false
	NovelBuilding.change_v = 1
	test_1()

func test_1():
	yield(get_tree().create_timer(1), "timeout")
	
	if(test_1_):
		test_1_l.visible = true
		test_1_r.visible = true
		test_1_ace.visible = true
	
	NovelBuilding.test_time_counter_v6(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2,test_1_bg_v3,test_1_bg_v4,test_1_bg_v5,test_1_bg_v6, right_ans)
	NovelBuilding.testLooping_v6(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2,test_1_bg_v3,test_1_bg_v4,test_1_bg_v5,test_1_bg_v6, right_ans)
	
	test_time.show()

#Botón para cambiar la visualización de dicha imagen gigante, para dar el movimiento del "Carrusel".
func _on_test_1_izq_pressed():
	if(test_1_):
		test_1_current_ans+=1
		if (test_1_current_ans>=5):
			test_1_current_ans=1
	fixing()

func _on_test_1_der_pressed():
	if(test_1_):
		test_1_current_ans-=1
		if (test_1_current_ans<=0):
			test_1_current_ans=4
	fixing()

func fixing():
	print(str("current: ",test_1_current_ans))
	
	if(NovelBuilding.change_v == 1):
		right_ans=2
		moving(test_1_bg_v1)
	if(NovelBuilding.change_v == 2):
		right_ans=3
		moving(test_1_bg_v2)
	if(NovelBuilding.change_v == 3):
		right_ans=3
		moving(test_1_bg_v3)
	if(NovelBuilding.change_v == 4):
		right_ans=3
		moving(test_1_bg_v4)
	if(NovelBuilding.change_v == 5):
		right_ans=1
		moving(test_1_bg_v5)
	if(NovelBuilding.change_v == 6):
		right_ans=4
		moving(test_1_bg_v6)

func moving(sp:Sprite):
	print(str("current: ",test_1_current_ans))
	if(test_1_current_ans==1):
		sp.position.x = 0
	if(test_1_current_ans==2):
		sp.position.x = -400
	if(test_1_current_ans==3):
		sp.position.x = -800
	if(test_1_current_ans==4):
		sp.position.x = -1200

#Cada fragmento de la imagne, que realmente es una opción a escoger, tiene un valor,
#este valor puede ser correcto (esto lo defeinen los desarrolladores), y así se sube
#a FireBase con el directorio 'estudiante'
func _on_test_1_aceptar_pressed():
	if(test_1_):
		print(str("ANSWER: ", test_1_current_ans))
		#Para v1, respuesta es 1
		if(test_1_current_ans == right_ans):
			NovelBuilding.endLoop = true
			http_noti.text = "Respuesta correcta."
			print(str("CORRECTO v1; salir del loop: ", NovelBuilding.endLoop))
			test_time.hide()
			perfil.puntajePrueba1 = {"stringValue": "CORRECTO"}
			perfil.codigoAula = {"integerValue": estudiante.fields.codigoAula.integerValue}
			perfil.estado = {"stringValue": "PRUEBA_1_OK"}
			perfil.nombreUsuario = {"stringValue": estudiante.fields.nombreUsuario.stringValue}
			perfil.progreso = {"integerValue": 1}
			perfil.puntajePrueba2 = {"stringValue": ""}
			perfil.puntajePrueba3 = {"stringValue": ""}
			perfil.puntajePrueba4 = {"stringValue": ""}
			perfil.puntajePrueba5 = {"stringValue": ""}
			http_noti.show()
			yield(get_tree().create_timer(2), "timeout")
			http_noti.text = "Guardando..."
			yield(get_tree().create_timer(1), "timeout")
			FireBase.update_document("estudiantes/%s" % FireBase.user_info.id, perfil, http)
			yield(get_tree().create_timer(2), "timeout")
			http_noti.hide()
			yield(get_tree().create_timer(1), "timeout")
			get_tree().change_scene("res://vista/cap_2.tscn")
		else:
			print(str("INCORRECTO v1"))
			perfil.puntajePrueba1 = {"stringValue": "INCORRECTO"}
			http_noti.show()
			http_noti.text = "Respuesta incorrecta."
			yield(get_tree().create_timer(4), "timeout")
			http_noti.hide()
	

#Control del mouse haciendo referencia a la variable 'interac' del NovelBuilding.
#Esto controla los cambios de escena y la continuidad de las conversaciones de los personajes
#en la novela.
func _input(event):
	if event is InputEventMouseButton:
		print(str("interact: ", NovelBuilding.interac))
		if NovelBuilding.interac == "CAP1.1" and event.pressed:
			cap_1_v2()
		if NovelBuilding.interac == "CAP1.2" and event.pressed and event.position.x >= 300 and event.position.x <= 400 and event.position.y >= 420 and event.position.y <= 620:
			print(str("POS: ", event.position.x,", ", event.position.y))
			NovelBuilding.stop_1 = true
			cap_1_v3()
		if NovelBuilding.interac == "CAP1.3" and event.pressed:
			conversation_1_v1()
		if NovelBuilding.interac == "CAP1.4" and event.pressed:
			conversation_1_v2()
		if NovelBuilding.interac == "CAP1.5" and event.pressed:
			conversation_1_v3()
		if NovelBuilding.interac == "CAP1.6" and event.pressed:
			conversation_1_v4()
		if NovelBuilding.interac == "CAP1.7" and event.pressed:
			conversation_1_v5()
		if NovelBuilding.interac == "CAP1.8" and event.pressed:
			conversation_1_v6()
		if NovelBuilding.interac == "CAP1.9" and event.pressed:
			conversation_1_v7()
		if NovelBuilding.interac == "conversation_2_v2" and event.pressed:
			conversation_2_v2()
		if NovelBuilding.interac == "test_1_intro" and event.pressed:
			test_1_intro()
			NovelBuilding.interac = "TEST1v2"

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	estudiante = result_body
	match response_code:
		404:
			print("400 cap1")
			http_noti.text = "Error al guardar la información."
			return
		200:
			print(str("Cargando... "))
