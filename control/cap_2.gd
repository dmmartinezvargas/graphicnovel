extends Node

onready var http: HTTPRequest = $HTTPRequest

onready var intro:Sprite = $inicio
onready var folleto_mama:Sprite = $folleto_mama
onready var folleto_perro:Sprite = $folleto_perro
onready var escritorio_pc:Sprite = $escritorio_pc
onready var uni_pag:Sprite = $uni_pag
onready var cabello:Sprite = $cabello
onready var lateral_perro:Sprite = $lateral_perro
onready var sad:Sprite = $sad
onready var tiempo_sol:Sprite = $tiempo_sol

onready var dialog_lb:Label = $dialog_lb
onready var dialog_bg:Sprite = $dialog_bg

onready var buscar_lb:Label = $buscar_lb
onready var buscador:TextEdit = $buscador
onready var buscar_repsuesta:Label = $buscar_respuesta
onready var button: Button = $Button

onready var test_1_l:Button = $left
onready var test_1_r:Button = $right
onready var test_1_ace:Button = $accept
onready var test_1_bg_v1: Sprite = $test_
onready var test_1_bg_v2: Sprite = $test_v2
onready var prueba_diag:Label = $enunc
onready var bg_:Sprite = $bg
onready var noti:Label = $noti

onready var test_time:Label = $test_time

var estudiante
var right_ans:int = 0
var test_1_current_ans:int = 1
var buscar:bool = false
var test_1_:bool = false

var perfil :={
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	FireBase.get_document("estudiantes/%s" % FireBase.user_info.id, http) #Aquí inicia el @Paso 1
	yield(get_tree().create_timer(4), "timeout")
	cap_2_v1()

func cap_2_v1():
	NovelBuilding._counter_showup(0,255,1,intro,0, "")
	NovelBuilding._movement_x_left_right(tiempo_sol, 450,200, 1, 9, 45)
	NovelBuilding._counter_showup(0,255,1,tiempo_sol,9, "")
	NovelBuilding._counter_showup(0,255,1,folleto_mama,9.1, "comment_1_v1")

func cap_2_commet_1_v1():
	tiempo_sol.hide()
	NovelBuilding._dialog_v1("SARA: \n¡Mamá, mira! ¡Te lo dije! Tienen diseño visual.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v2"

func cap_2_commet_1_v2():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \nSara, ya hemos hablado de esto, espera hasta el \npróximo año.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_1_v3"

func cap_2_commet_1_v3():
	NovelBuilding._dialog_v1("SARA: \nEso me dijiste el año pasado, y aún sigo aquí con la \nabuela y contigo.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="cap2.2"

func cap_2_v2():
	NovelBuilding._dialog_v1("SARA: \nBuscaré si tienen la carrera que a mi me gusta.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	NovelBuilding._counter_showup(0,255,1,escritorio_pc,3, "buscador_part")

func input_text():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,uni_pag,3, "")
	yield(get_tree().create_timer(7), "timeout")
	buscar_lb.show()
	buscador.show()
	buscar_repsuesta.show()
	buscar=true
	button.show()

func input_response():
	yield(get_tree().create_timer(2), "timeout")
	buscar_lb.hide()
	buscador.hide()
	buscar_repsuesta.hide()
	button.hide()
	NovelBuilding._dialog_v1("SARA: \nDesde que me gradué no he hecho sino ver como \nlos demás salen adelante y yo sigo aquí estancada… \natrapada. Necesito un cambio.",
		20,25,20, 5,2.1, dialog_bg,dialog_lb)
	NovelBuilding._counter_showup(0,255,1,cabello,2, "comment_2_v2")
	yield(get_tree().create_timer(1), "timeout")

func commmet_2_v2():
	NovelBuilding._dialog_v1("SARA: \nYa sé. Pero tendré que ir a la peluquería y sólo \ntengo 30 minutos antes de que mamá vuelva y la \npeluquería está a 5.",
		20,25,20, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="reto_1"

func reto_():
	dialog_bg.hide()
	dialog_lb.hide()
	print(str("TEST 2"))
	NovelBuilding._counter_showup(0,255,1,bg_, 1, "TEST1v2")
	yield(get_tree().create_timer(2), "timeout")
	prueba_diag.text ="¡Para continuar, debes pasar este reto!"
	prueba_diag.show()
	test_1_ = true;
	yield(get_tree().create_timer(2), "timeout")
	NovelBuilding.endLoop = false
	NovelBuilding.change_v = 1
	reto_v2()
	

func reto_v2():
	yield(get_tree().create_timer(1), "timeout")
	
	if(test_1_):
		test_1_l.visible = true
		test_1_r.visible = true
		test_1_ace.visible = true
	
	NovelBuilding.test_time_counter(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2, right_ans)
	NovelBuilding.testLooping(1.3, 45, test_time, test_1_bg_v1, test_1_bg_v2, right_ans)
	
	test_time.show()

func _on_left_pressed():
	if(test_1_):
		test_1_current_ans+=1
		if (test_1_current_ans>=7):
			test_1_current_ans=1
	fixing()

func _on_right_pressed():
	if(test_1_):
		test_1_current_ans-=1
		if (test_1_current_ans<=0):
			test_1_current_ans=6
	fixing()

func fixing():
	
	if(NovelBuilding.change_v == 1):
		right_ans=5
		moving(test_1_bg_v1)
	if(NovelBuilding.change_v == 2):
		right_ans=2
		moving(test_1_bg_v2)

func moving(sp:Sprite):
	print(str("current: ",test_1_current_ans))
	if(test_1_current_ans==1):
		sp.position.x = 0
	if(test_1_current_ans==2):
		sp.position.x = -400
	if(test_1_current_ans==3):
		sp.position.x = -800
	if(test_1_current_ans==4):
		sp.position.x = -1200
	if(test_1_current_ans==5):
		sp.position.x = -1600
	if(test_1_current_ans==6):
		sp.position.x = -2000

func _on_accept_pressed():
	if(test_1_):
		print(str("ANSWER: ", test_1_current_ans))
		#Para v1, respuesta es 1
		if(test_1_current_ans == right_ans):
			NovelBuilding.endLoop = true
			noti.text = "Respuesta correcta."
			print(str("CORRECTO v1; salir del loop: ", NovelBuilding.endLoop))
			test_time.hide()
			perfil.puntajePrueba2 = {"stringValue": "CORRECTO"}
			perfil.codigoAula = {"integerValue": estudiante.fields.codigoAula.integerValue}
			perfil.estado = {"stringValue": "PRUEBA_2_OK"}
			perfil.nombreUsuario = {"stringValue": estudiante.fields.nombreUsuario.stringValue}
			perfil.progreso = {"integerValue": 2}
			perfil.puntajePrueba1 = {"stringValue": estudiante.fields.puntajePrueba1.stringValue}
			perfil.puntajePrueba3 = {"stringValue": ""}
			perfil.puntajePrueba4 = {"stringValue": ""}
			perfil.puntajePrueba5 = {"stringValue": ""}
			noti.show()
			yield(get_tree().create_timer(2), "timeout")
			noti.text = "Guardando..."
			yield(get_tree().create_timer(1), "timeout")
			FireBase.update_document("estudiantes/%s" % FireBase.user_info.id, perfil, http)
			yield(get_tree().create_timer(2), "timeout")
			noti.hide()
			yield(get_tree().create_timer(1), "timeout")
			post_reto()
		else:
			print(str("INCORRECTO v1"))
			perfil.puntajePrueba2 = {"stringValue": "INCORRECTO"}
			noti.show()
			noti.text = "Respuesta incorrecta."


func post_reto():
	yield(get_tree().create_timer(1), "timeout")
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,lateral_perro,2, "comment_3_v1")

func comment_3_v1():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-No es fácil para mí [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v2"

func comment_3_v2():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-Lo dices porque no estás aquí [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v3"

func comment_3_v3():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-Ella se merece lo mejor [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v4"

func comment_3_v4():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-Me siento un fracaso como madre [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v5"

func comment_3_v5():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-Y se supone que tú eres su padre y [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v6"

func comment_3_v6():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-Nunca ayudaste [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="comment_3_v7"

func comment_3_v7():
	NovelBuilding._dialog_v1("MAMÁ DE SARA: \n[HABLANDO POR TELÉFONO]\n\n-Y ahora mucho menos lo puedes hacer [...]",
		20,225,220, 5,0.5, dialog_bg,dialog_lb)
	yield(get_tree().create_timer(1), "timeout")
	NovelBuilding.interac="cap2_v3"

func cap2_v3():
	dialog_bg.hide()
	dialog_lb.hide()
	NovelBuilding._counter_showup(0,255,1,sad,1, "next_cap")

func next_cap_():
	get_tree().change_scene("res://vista/cap_3.tscn")

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	estudiante = result_body
	match response_code:
		404:
			print("400")
			return
		200:
			print(str("Cargando... "))

func _input(event):	
	if event is InputEventMouseButton:
		print(str("timeleft: ", NovelBuilding.testTime,"barSize: ", NovelBuilding.barSize))
		if NovelBuilding.interac == "comment_1_v1" and event.pressed:
			cap_2_commet_1_v1()
		if NovelBuilding.interac == "comment_1_v2" and event.pressed:
			cap_2_commet_1_v2()
		if NovelBuilding.interac == "comment_1_v3" and event.pressed:
			cap_2_commet_1_v3()
		if NovelBuilding.interac == "cap2.2" and event.pressed:
			cap_2_v2()
		if NovelBuilding.interac == "buscador_part" and event.pressed:
			input_text()
		#if NovelBuilding.interac == "comment_2_v1" and event.pressed:
			#commmet_2_v1()
		if NovelBuilding.interac == "comment_2_v2" and event.pressed:
			commmet_2_v2()
		if NovelBuilding.interac == "reto_1" and event.pressed:
			reto_()
		if NovelBuilding.interac == "comment_3_v1" and event.pressed:
			comment_3_v1()
		if NovelBuilding.interac == "comment_3_v2" and event.pressed:
			comment_3_v2()
		if NovelBuilding.interac == "comment_3_v3" and event.pressed:
			comment_3_v3()
		if NovelBuilding.interac == "comment_3_v4" and event.pressed:
			comment_3_v4()
		if NovelBuilding.interac == "comment_3_v5" and event.pressed:
			comment_3_v5()
		if NovelBuilding.interac == "comment_3_v6" and event.pressed:
			comment_3_v6()
		if NovelBuilding.interac == "comment_3_v7" and event.pressed:
			comment_3_v7()
		if NovelBuilding.interac == "cap2_v3" and event.pressed:
			cap2_v3()
		if NovelBuilding.interac == "next_cap" and event.pressed:
			next_cap_()


func _on_Button_pressed():
	if(buscar):
		if(buscador.text == "diseño visual" || buscador.text == "diseño   visual" || buscador.text == "diseño  visual" || buscador.text == "DISEÑO VISUAL" || buscador.text == "Diseño visual" || buscador.text == "Diseño Visual" || buscador.text == "diseño Visual" || buscador.text == "diseñovisual"
		|| buscador.text == "diseño gráfico" || buscador.text == "diseño grafico" || buscador.text == "DISEÑO GRÁFICO" || buscador.text == "DISEÑO GRAFICO" || buscador.text == "diseñográfico" || buscador.text == "diseñografico" || buscador.text == "diseño  gráfico" || buscador.text == "diseño  grafico" || buscador.text == "Diseño gráfico" || buscador.text == "Diseño grafico" || buscador.text == "diseño Gráfico" || buscador.text == "diseño Grafico" || buscador.text == "Diseño Gráfico" || buscador.text == "Diseño Grafico"):
			buscar_repsuesta.text="buscando..."
			input_response()
			buscar=false
			print(str("respuesta correcta"))
		else:
			buscar_repsuesta.text="Escribe la carrera que Sara quiere estudiar."
			print(str("respuesta correcta"))
