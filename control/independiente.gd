extends Node2D

onready var http: HTTPRequest = $HTTPRequest
onready var nombreEstudante: TextEdit = $nombreUsuario
onready var notificacion: Label = $notificacion

var estudiante:= {
	"codigoAula":{},
	"nombreUsuario": {},
	"puntajePrueba1": {},
	"puntajePrueba2": {},
	"puntajePrueba3": {},
	"puntajePrueba4": {},
	"puntajePrueba5": {},
	"progreso": {},
	"estado": {},
}

func _ready():
	FireBase.get_document("estudiantes", http)

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var result_body := JSON.parse(body.get_string_from_ascii()).result as Dictionary
	match response_code:
		404:
			print("400")
			notificacion.text = "Error en servidor."
		200:
			print("200 indep")

func _on_atras_pressed():
	get_tree().change_scene("res://vista/aulaconfig.tscn")

func _on_inicio_pressed():
	print(str("id del independiente; ",FireBase.user_info.id))
	
	if nombreEstudante.text.empty():
		notificacion.text = "Por favor, llena todos los campos."
	else:
		estudiante.codigoAula ={"integerValue": 0}
		estudiante.nombreUsuario = {"stringValue": nombreEstudante.text}
		estudiante.puntajePrueba1 = {"stringValue": ""}
		estudiante.puntajePrueba2 = {"stringValue": ""}
		estudiante.puntajePrueba3 = {"stringValue": ""}
		estudiante.puntajePrueba4 = {"stringValue": ""}
		estudiante.puntajePrueba5 = {"stringValue": ""}
		estudiante.progreso = {"integerValue": 0}
		estudiante.estado = {"stringValue": "INICIO"}
		estudiante.tipo = {"stringValue": "INDEPENDIENTE"}
		print(str("try?"))
		yield(get_tree().create_timer(1), "timeout")
		FireBase.save_document("estudiantes?documentId=%s" % FireBase.user_info.id , estudiante, http)
		print(str("saving?"))
		notificacion.text = "Accediendo..."
		yield(get_tree().create_timer(1), "timeout")
		print(str("leaving"))
		get_tree().change_scene("res://vista/cap_1.tscn")


