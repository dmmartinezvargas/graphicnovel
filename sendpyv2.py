import email, smtplib, ssl

from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import os
#reference:  https://realpython.com/python-send-email/

#f = open("getter.txt", "r")

subject = "Exportación de información - Factor"
body = "Archivos adjuntos contienen el resumen de aulas y estudiantes. final 1"
sender_email = "tgnovel.v1@gmail.com"
#receiver_email = f.read()
receiver_email = "maurtz.nm@gmail.com"
password = "Reconstitucion4"
#f.close()

message = MIMEMultipart()
message["From"] = sender_email
message["To"] = receiver_email
message["Subject"] = subject
message["Bcc"] = receiver_email

message.attach(MIMEText(body, "plain"))

fileStudents = "data/data_estudiantes.csv"
fileClassrooms = "data/data_aulas.csv"
print(str("sending..."))
# Open PDF file in binary mode
with open(fileStudents, "rb") as attachment:
    # Add file as application/octet-stream
    # Email client can usually download this automatically as attachment
    part = MIMEBase("application", "octet-stream")
    part.set_payload(attachment.read())

encoders.encode_base64(part)

with open(fileClassrooms, "rb") as attachment:
    # Add file as application/octet-stream
    # Email client can usually download this automatically as attachment
    part2 = MIMEBase("application", "octet-stream")
    part2.set_payload(attachment.read())

# Encode file in ASCII characters to send by email    
encoders.encode_base64(part2)

# Add header as key/value pair to attachment part
part.add_header(
    "Content-Disposition",
    f"attachment; filename= {fileStudents}",
)

part2.add_header(
    "Content-Disposition",
    f"attachment; filename= {fileClassrooms}",
)


message.attach(part2)
message.attach(part)
text = message.as_string()

context = ssl.create_default_context()
with smtplib.SMTP_SSL("smtp.gmail.com", 465, context=context) as server:
    server.login(sender_email, password)
    server.sendmail(sender_email, receiver_email, text)

#os.remove("getter.txt")