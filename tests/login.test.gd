extends WAT.Test

var id = FireBase.PROJECT_ID
var key = FireBase.API_KEY
var registerId = FireBase.REGISTER_URL
var loginId = FireBase.LOGIN_URL
var firebaseId = FireBase.FIRESTORE_URL

func test_firebase_credentiales_no_vacias():
	asserts.is_not_equal("",key)
	asserts.is_not_equal("",id)

func test_firebase_contiene_proyectoid():
	asserts.string_contains(id, firebaseId)

func test_direcciones_contiene_key():
	asserts.string_contains(key, registerId)
	asserts.string_contains(key, loginId)
